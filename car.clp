(defrule carDiagnostic
    =>
    (printout t "Does car start? (1-yes, 0-no)" crlf)
    (bind ?x (read))
    (if (= ?x 1)
        then
        (assert (car-turn-on yes))
        else
        (assert (car-turn-on no))
    )
)

(defrule starterOn
    (car-turn-on yes)
    =>
    (printout t "Solution: no need to repair." crlf)
)

(defrule sraterOff
    (car-turn-on no)
    =>
    (printout t "Does the started works?" crlf)
    (bind ?x (read))
    (if (= ?x 1)
    then(printout t "Solution: Check you have gas." crlf)
    else(printout t "Solution: Check headlight." crlf)
    )
)