; ----------------------------
;  Eugen Caruntu, 29077103
; ----------------------------

; ===========================================================================
;  FUNCTIONS
; ===========================================================================

(deffunction ask-question (?q ?v)
   (printout t ?q ?v crlf)
   (bind ?answer (read))
   (while (not (member ?answer ?v)) do
      (printout t "The entered value won't match any gem. Please retry." crlf ?q ?v crlf)
      (bind ?answer (read)))
   ?answer)
   
(deffunction askcontinue (?s)
    (printout t "Would you like to continue identifying gems? (yes, no) " ?s crlf)
        (bind ?a (read))
        (while (and (neq ?a yes) (neq ?a no)) do
            (printout t "The answer was invalid. Would you like to continue identifying gems? (yes, no) " crlf)
            (bind ?a (read)))
            (assert (continue ?a)))

; ===========================================================================
; QUESTIONS
; ===========================================================================

(defrule welcome "The questions to ask"
    ?f <- (initial-fact)
	=> (retract ?f)
    (printout t "=========================================" crlf)
    (printout t " This Expert System can help you with:" 
                    crlf tab "Gem Identification" crlf)
    (printout t "=========================================" crlf)
    (assert (reload questions)))

(defrule reload-questions
    ?f <- (reload questions)
    => (retract ?f)
    (assert (question hardness "What hardness? " (create$ 7 8 8.5 9 10)))
    (assert (question density "What density? " (create$ 2.65 3.1 3.52 3.6 3.72 4)))
    (assert (question color "What color? " (create$ black blue brown green pink red violet white yellow colorless))))

; ===========================================================================
; RULES TO HANDLE QUESTIONING
; ===========================================================================

(defrule question-text "As all text answerable questions that were not asked already and make attribute facts from the answers"
    ?f <- (question ?a ?q $?v)
    => (retract ?f)
    (assert (attribute ?a (ask-question ?q ?v) undecided)))

; ===========================================================================
;  CONTROLL RULES
; ===========================================================================

(defrule continue-ask "Ask user to continue"
    ?f <- (success yes|no)
    => (retract ?f)
    (askcontinue "?"))

(defrule continue-yes "Reset the state if user wants to continue"
    ?f <- (continue yes)
    => (retract ?f)
    (do-for-all-facts ((?f attribute)) TRUE (retract ?f))
    (assert (reload questions)))

(defrule continue-no "Exit the application once user does not want to continue"
    ?f <- (continue no)
    => (retract ?f)
    (do-for-all-facts ((?f attribute)) TRUE (retract ?f))
    (printout t "Goodbye." crlf)
    (halt))

; ===========================================================================
;  GEM SPECIFIC RULES
; ===========================================================================

(defrule Diamond
    (or ?f <- (attribute ?a&:(eq ?a hardness) ?v&:(eq ?v 10) ?n&:(and (eq ?n undecided) (neq Diamond ?n)))
        ?f <- (attribute ?a&:(eq ?a density) ?v&:(eq ?v 3.52) ?n&:(and (eq ?n undecided) (neq Diamond ?n)))
        ?f <- (attribute ?a&:(eq ?a color) ?v&:(member ?v (create$ yellow brown green blue white colorless)) ?n&:(and (eq ?n undecided) (neq Diamond ?n))))
    =>  (assert (attribute ?a ?v Diamond)))

(defrule Corundum
    (or ?f <- (attribute ?a&:(eq ?a hardness) ?v&:(eq ?v 9) ?n&:(and (eq ?n undecided) (neq Corundum ?n)))
        ?f <- (attribute ?a&:(eq ?a density) ?v&:(eq ?v 4) ?n&:(and (eq ?n undecided) (neq Corundum ?n)))
        ?f <- (attribute ?a&:(eq ?a color) ?v&:(member ?v (create$ red pink yellow brown green blue violet black white colorless)) ?n&:(and (eq ?n undecided) (neq Corundum ?n))))
    =>  (assert (attribute ?a ?v Corundum)))

(defrule Chrysoberyl
    (or ?f <- (attribute ?a&:(eq ?a hardness) ?v&:(eq ?v 8.5) ?n&:(and (eq ?n undecided) (neq Chrysoberyl ?n)))
        ?f <- (attribute ?a&:(eq ?a density) ?v&:(eq ?v 3.72) ?n&:(and (eq ?n undecided) (neq Chrysoberyl ?n)))
        ?f <- (attribute ?a&:(eq ?a color) ?v&:(member ?v (create$ yellow brown green)) ?n&:(and (eq ?n undecided) (neq Chrysoberyl ?n))))
    =>  (assert (attribute ?a ?v Chrysoberyl)))

(defrule Spinel
    (or ?f <- (attribute ?a&:(eq ?a hardness) ?v&:(eq ?v 8) ?n&:(and (eq ?n undecided) (neq Spinel ?n)))
        ?f <- (attribute ?a&:(eq ?a density) ?v&:(eq ?v 3.6) ?n&:(and (eq ?n undecided) (neq Spinel ?n)))
        ?f <- (attribute ?a&:(eq ?a color) ?v&:(member ?v (create$ red pink yellow brown green blue violet white colorless)) ?n&:(and (eq ?n undecided) (neq Spinel ?n))))
    =>  (assert (attribute ?a ?v Spinel)))

(defrule Quartz
    (or ?f <- (attribute ?a&:(eq ?a hardness) ?v&:(eq ?v 7) ?n&:(and (eq ?n undecided) (neq Quartz ?n)))
        ?f <- (attribute ?a&:(eq ?a density) ?v&:(eq ?v 2.65) ?n&:(and (eq ?n undecided) (neq Quartz ?n)))
        ?f <- (attribute ?a&:(eq ?a color) ?v&:(member ?v (create$ red pink green blue violet white black colorless)) ?n&:(and (eq ?n undecided) (neq Quartz ?n))))
    =>  (assert (attribute ?a ?v Quartz)))

(defrule Tourmaline
    (or ?f <- (attribute ?a&:(eq ?a hardness) ?v&:(eq ?v 7) ?n&:(and (eq ?n undecided) (neq Tourmaline ?n)))
        ?f <- (attribute ?a&:(eq ?a density) ?v&:(eq ?v 3.1) ?n&:(and (eq ?n undecided) (neq Tourmaline ?n)))
        ?f <- (attribute ?a&:(eq ?a color) ?v&:(member ?v (create$ red pink yellow brown green blue white black colorless)) ?n&:(and (eq ?n undecided) (neq Tourmaline ?n))))
    =>  (assert (attribute ?a ?v Tourmaline)))

; ===========================================================================
;  RULES TO IDENTIFY GEMS
; ===========================================================================

(defrule identify-gem
	?f1 <- (attribute hardness ?v1 ?g&:(neq ?g undecided))
	?f2 <- (attribute density ?v2 ?g&:(neq ?g undecided))
	?f3 <- (attribute color ?v3 ?g&:(neq ?g undecided))
	=> (retract ?f1 ?f2 ?f3)
    (printout t "The gem is " ?g crlf)
	(assert (success yes)))

(defrule none-matched "Rule to set failure"
	?f1 <- (attribute hardness ?v1 ?g&:(eq ?g undecided))
	?f2 <- (attribute density ?v2 ?g&:(eq ?g undecided))
	?f3 <- (attribute color ?v3 ?g&:(eq ?g undecided))
	=> (retract ?f1 ?f2 ?f3)
    (printout t "There was no gem matched!" crlf)
    (assert (success no)))