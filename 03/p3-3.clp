; ----------------------------
;  Eugen Caruntu, 29077103
; ----------------------------

; ===========================================================================
; FUNCTIONS
; ===========================================================================

(deffunction ask-question-text (?q ?v)
   (printout t ?q ?v crlf)
   (bind ?answer (read))
   (while (not (member ?answer ?v)) do
      (printout t "The value must be one of the indicated values. Please retry." crlf ?q ?v crlf)
      (bind ?answer (read)))
   ?answer)

(deffunction ask-question-ranges (?q ?lower ?upper)
   (printout t ?q "[" ?lower " - " ?upper"]" crlf)
   (bind ?answer (read))
   (while (or (not (numberp ?answer))
              (< ?answer ?lower)
              (> ?answer ?upper)) do
      (printout t "The value must be between indicated range. Please retry." crlf ?q "[" ?lower " - " ?upper"]" crlf)
      (bind ?answer (read)))
   ?answer)

(deffunction askcontinue (?s)
    (printout t "Would you like to continue identifying gems? [yes, no]" ?s crlf)
        (bind ?a (read))
        (while (and (neq ?a yes) (neq ?a no)) do
            (printout t "The answer was invalid. Would you like to continue identifying gems? [yes, no]" crlf)
            (bind ?a (read)))
            (assert (continue ?a)))

; ===========================================================================
; QUESTIONS
; ===========================================================================
        
(defrule welcome "The questions to ask"
    ?f <- (initial-fact)
	=> (retract ?f)
    (printout t "=========================================" crlf)
    (printout t " This Expert System can help you with:" 
                    crlf tab "Gem Identification" crlf)
    (printout t "=========================================" crlf)
    (assert (reload questions)))

(defrule reload-questions
    ?f <- (reload questions)
    => (retract ?f)
    (assert (question color "What color? " txt (create$ black blue brown green pink red violet white yellow colorless)))
    (assert (question density "What density? " rng 1 6))
    (assert (question hardness "What hardness? " rng 1 10))
    (set-strategy mea))

; ===========================================================================
; RULES TO HANDLE QUESTIONING
; ===========================================================================

(defrule question-text "As all text answerable questions that were not asked already and make attribute facts from the answers"
    ?f <- (question ?a ?q txt $?v)
    => (retract ?f)
    (assert (attribute ?a (ask-question-text ?q ?v) undecided)))

(defrule question-ranges "As all number range answerable questions that were not asked already and make attribute facts from the answers"
    ?f <- (question ?a ?q rng ?lower ?upper)
    => (retract ?f)
    (assert (attribute ?a (ask-question-ranges ?q ?lower ?upper) undecided)))

; ===========================================================================
;  CONTROLL RULES
; ===========================================================================

(defrule continue-ask "Ask user to continue"
    ?f <- (success yes|no)
    => (retract ?f)
    (do-for-all-facts ((?f attribute)) TRUE (retract ?f))
    (askcontinue "?"))

(defrule continue-yes "Mark questions for reset if user wants to continue"
    ?f <- (continue yes)
    => (retract ?f)
    (assert (reload questions)))

(defrule continue-no "Exit the application once user does not want to continue"
    ?f <- (continue no)
    => (retract ?f)
    (printout t "Goodbye." crlf)
    (halt))

; ===========================================================================
;  GEM SPECIFIC RULES
; ===========================================================================

(defrule Diamond
    (or ?f <- (attribute ?a&:(eq ?a hardness) ?v&:(and (>= ?v 9.9) (<= ?v 10.1)) ?n&:(and (eq ?n undecided) (neq Diamond ?n)))
        ?f <- (attribute ?a&:(eq ?a density) ?v&:(and (>= ?v 3.42) (<= ?v 3.62)) ?n&:(and (eq ?n undecided) (neq Diamond ?n)))
        ?f <- (attribute ?a&:(eq ?a color) ?v&:(member ?v (create$ yellow brown green blue white colorless)) ?n&:(and (eq ?n undecided) (neq Diamond ?n))))
    =>  (assert (attribute ?a ?v Diamond)))

 (defrule Corundum
    (or ?f <- (attribute ?a&:(eq ?a hardness) ?v&:(and (>= ?v 8.9) (<= ?v 9.1)) ?n&:(and (eq ?n undecided) (neq Corundum ?n)))
        ?f <- (attribute ?a&:(eq ?a density) ?v&:(and (>= ?v 3.9) (<= ?v 4.1)) ?n&:(and (eq ?n undecided) (neq Corundum ?n)))
        ?f <- (attribute ?a&:(eq ?a color) ?v&:(member ?v (create$ red pink yellow brown green blue violet black white colorless)) ?n&:(and (eq ?n undecided) (neq Corundum ?n))))
    =>  (assert (attribute ?a ?v Corundum)))

(defrule Chrysoberyl
    (or ?f <- (attribute ?a&:(eq ?a hardness) ?v&:(and (>= ?v 8.4) (<= ?v 8.6)) ?n&:(and (eq ?n undecided) (neq Chrysoberyl ?n)))
        ?f <- (attribute ?a&:(eq ?a density) ?v&:(and (>= ?v 3.62) (<= ?v 3.82)) ?n&:(and (eq ?n undecided) (neq Chrysoberyl ?n)))
        ?f <- (attribute ?a&:(eq ?a color) ?v&:(member ?v (create$ yellow brown green)) ?n&:(and (eq ?n undecided) (neq Chrysoberyl ?n))))
    =>  (assert (attribute ?a ?v Chrysoberyl)))

(defrule Spinel
    (or ?f <- (attribute ?a&:(eq ?a hardness) ?v&:(and (>= ?v 7.9) (<= ?v 8.1)) ?n&:(and (eq ?n undecided) (neq Spinel ?n)))
        ?f <- (attribute ?a&:(eq ?a density) ?v&:(and (>= ?v 3.5) (<= ?v 3.7)) ?n&:(and (eq ?n undecided) (neq Spinel ?n)))
        ?f <- (attribute ?a&:(eq ?a color) ?v&:(member ?v (create$ red pink yellow brown green blue violet white colorless)) ?n&:(and (eq ?n undecided) (neq Spinel ?n))))
    =>  (assert (attribute ?a ?v Spinel)))
    
(defrule Topaz
    (or ?f <- (attribute ?a&:(eq ?a hardness) ?v&:(and (>= ?v 7.9) (<= ?v 8.1)) ?n&:(and (eq ?n undecided) (neq Topaz ?n)))
        ?f <- (attribute ?a&:(eq ?a density) ?v&:(and (>= ?v 3.52) (<= ?v 3.56)) ?n&:(and (eq ?n undecided) (neq Topaz ?n)))
        ?f <- (attribute ?a&:(eq ?a color) ?v&:(member ?v (create$ red pink yellow brown blue violet white colorless)) ?n&:(and (eq ?n undecided) (neq Topaz ?n))))
    =>  (assert (attribute ?a ?v Topaz)))

(defrule Beryl
    (or ?f <- (attribute ?a&:(eq ?a hardness) ?v&:(and (>= ?v 7.5) (<= ?v 8.0)) ?n&:(and (eq ?n undecided) (neq Beryl ?n)))
        ?f <- (attribute ?a&:(eq ?a density) ?v&:(and (>= ?v 2.6) (<= ?v 2.8)) ?n&:(and (eq ?n undecided) (neq Beryl ?n)))
        ?f <- (attribute ?a&:(eq ?a color) ?v&:(member ?v (create$ red pink yellow brown green blue white colorless)) ?n&:(and (eq ?n undecided) (neq Beryl ?n))))
    =>  (assert (attribute ?a ?v Beryl)))
    
(defrule Zircon
    (or ?f <- (attribute ?a&:(eq ?a hardness) ?v&:(and (>= ?v 6) (<= ?v 7.5)) ?n&:(and (eq ?n undecided) (neq Zircon ?n)))
        ?f <- (attribute ?a&:(eq ?a density) ?v&:(and (>= ?v 4.6) (<= ?v 4.8)) ?n&:(and (eq ?n undecided) (neq Zircon ?n)))
        ?f <- (attribute ?a&:(eq ?a color) ?v&:(member ?v (create$ yellow brown green violet white colorless)) ?n&:(and (eq ?n undecided) (neq Zircon ?n))))
    =>  (assert (attribute ?a ?v Zircon)))

(defrule Quartz
    (or ?f <- (attribute ?a&:(eq ?a hardness) ?v&:(and (>= ?v 6.9) (<= ?v 7.1)) ?n&:(and (eq ?n undecided) (neq Quartz ?n)))
        ?f <- (attribute ?a&:(eq ?a density) ?v&:(and (>= ?v 2.55) (<= ?v 2.75)) ?n&:(and (eq ?n undecided) (neq Quartz ?n)))
        ?f <- (attribute ?a&:(eq ?a color) ?v&:(member ?v (create$ red pink green blue violet white black colorless)) ?n&:(and (eq ?n undecided) (neq Quartz ?n))))
    =>  (assert (attribute ?a ?v Quartz)))

(defrule Tourmaline
    (or ?f <- (attribute ?a&:(eq ?a hardness) ?v&:(and (>= ?v 6.9) (<= ?v 7.1)) ?n&:(and (eq ?n undecided) (neq Tourmaline ?n)))
        ?f <- (attribute ?a&:(eq ?a density) ?v&:(and (>= ?v 3.0) (<= ?v 3.2)) ?n&:(and (eq ?n undecided) (neq Tourmaline ?n)))
        ?f <- (attribute ?a&:(eq ?a color) ?v&:(member ?v (create$ red pink yellow brown green blue white black colorless)) ?n&:(and (eq ?n undecided) (neq Tourmaline ?n))))
    =>  (assert (attribute ?a ?v Tourmaline)))
    
(defrule Peridot
    (or ?f <- (attribute ?a&:(eq ?a hardness) ?v&:(and (>= ?v 6.5) (<= ?v 7)) ?n&:(and (eq ?n undecided) (neq Peridot ?n)))
        ?f <- (attribute ?a&:(eq ?a density) ?v&:(and (>= ?v 3.2) (<= ?v 3.4)) ?n&:(and (eq ?n undecided) (neq Peridot ?n)))
        ?f <- (attribute ?a&:(eq ?a color) ?v&:(member ?v (create$ yellow brown green)) ?n&:(and (eq ?n undecided) (neq Peridot ?n))))
    =>  (assert (attribute ?a ?v Peridot)))

(defrule Jadeite
    (or ?f <- (attribute ?a&:(eq ?a hardness) ?v&:(and (>= ?v 6.5) (<= ?v 7)) ?n&:(and (eq ?n undecided) (neq Jadeite ?n)))
        ?f <- (attribute ?a&:(eq ?a density) ?v&:(and (>= ?v 3.2) (<= ?v 3.4)) ?n&:(and (eq ?n undecided) (neq Jadeite ?n)))
        ?f <- (attribute ?a&:(eq ?a color) ?v&:(member ?v (create$ red pink yellow brown green blue violet white black colorless)) ?n&:(and (eq ?n undecided) (neq Jadeite ?n))))
    =>  (assert (attribute ?a ?v Jadeite)))

(defrule Opal
    (or ?f <- (attribute ?a&:(eq ?a hardness) ?v&:(and (>= ?v 5.5) (<= ?v 6.5)) ?n&:(and (eq ?n undecided) (neq Opal ?n)))
        ?f <- (attribute ?a&:(eq ?a density) ?v&:(and (>= ?v 2) (<= ?v 2.2)) ?n&:(and (eq ?n undecided) (neq Opal ?n)))
        ?f <- (attribute ?a&:(eq ?a color) ?v&:(member ?v (create$ red pink yellow brown white black colorless)) ?n&:(and (eq ?n undecided) (neq Opal ?n))))
    =>  (assert (attribute ?a ?v Opal)))   

(defrule Nephrite
    (or ?f <- (attribute ?a&:(eq ?a hardness) ?v&:(and (>= ?v 5) (<= ?v 6)) ?n&:(and (eq ?n undecided) (neq Nephrite ?n)))
        ?f <- (attribute ?a&:(eq ?a density) ?v&:(and (>= ?v 2.9) (<= ?v 3.4)) ?n&:(and (eq ?n undecided) (neq Nephrite ?n)))
        ?f <- (attribute ?a&:(eq ?a color) ?v&:(member ?v (create$ green white black colorless)) ?n&:(and (eq ?n undecided) (neq Nephrite ?n))))
    =>  (assert (attribute ?a ?v Nephrite)))    

(defrule Turquoise
    (or ?f <- (attribute ?a&:(eq ?a hardness) ?v&:(and (>= ?v 5) (<= ?v 6)) ?n&:(and (eq ?n undecided) (neq Turquoise ?n)))
        ?f <- (attribute ?a&:(eq ?a density) ?v&:(and (>= ?v 2.6) (<= ?v 2.8)) ?n&:(and (eq ?n undecided) (neq Turquoise ?n)))
        ?f <- (attribute ?a&:(eq ?a color) ?v&:(member ?v (create$ blue)) ?n&:(and (eq ?n undecided) (neq Turquoise ?n))))
    =>  (assert (attribute ?a ?v Turquoise)))
    
; ===========================================================================
;  RULES TO IDENTIFY GEMS
; ===========================================================================

(defrule identify-gem
	?f1 <- (attribute hardness ?v1 ?g&:(neq ?g undecided))
	?f2 <- (attribute density ?v2 ?g&:(neq ?g undecided))
	?f3 <- (attribute color ?v3 ?g&:(neq ?g undecided))
	=> (retract ?f1 ?f2 ?f3)
    (printout t "The gem is " ?g crlf)
	(assert (exact)))

(defrule allow-multiple-exact-topics "Set strategy to breadth to allow for any multiple exact matches"
    ?f0 <- (exact)
    (not (success yes))
    ?f1 <- (attribute hardness ?v1 ?g&:(eq ?g undecided))
	?f2 <- (attribute density ?v2 ?g&:(eq ?g undecided))
	?f3 <- (attribute color ?v3 ?g&:(eq ?g undecided))
	=> (retract ?f0 ?f1 ?f2 ?f3)
    ; (set-strategy breadth)
    (assert (success yes)))

(defrule none-matched "Rule to set failure"
    (not (exact))
    (not (success yes))
	?f1 <- (attribute hardness ?v1 ?g&:(eq ?g undecided))
	?f2 <- (attribute density ?v2 ?g&:(eq ?g undecided))
	?f3 <- (attribute color ?v3 ?g&:(eq ?g undecided))
	=> (retract ?f1 ?f2 ?f3)
    (printout t "There was no gem matched!" crlf)
    (assert (success no)))
