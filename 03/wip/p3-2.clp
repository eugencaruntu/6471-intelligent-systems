; ----------------------------
;  Eugen Caruntu, 29077103
; ----------------------------

; ===========================================================================
; DEFTEMPLATES
; ===========================================================================

(deftemplate attribute "Intermediate attributes used to build a gem from user answers"
    (slot name (allowed-values hardness density color))
    (slot val))

(deftemplate gem "The gem template"
   (slot name)
   (slot hardness)
   (slot density)
   (multislot color (allowed-values black blue brown green pink red violet white yellow colorless)))

(deftemplate question "The questions template related to gem attributes"
    (slot asked (default no))
    (slot atr (allowed-values hardness density color))
    (slot the-question (type STRING))
    (multislot valid))

; ===========================================================================
;  FUNCTIONS
; ===========================================================================

(deffunction ask-question (?q ?v)
   (printout t ?q ?v crlf)
   (bind ?answer (read))
   (while (not (member ?answer ?v)) do
      (printout t "The entered value wont match any gem. Please retry." crlf ?q ?v crlf)
      (bind ?answer (read)))
   ?answer)

; ===========================================================================
; QUESTION FACTS
; ===========================================================================

(deffacts questions "The questions to ask"
    (question (atr hardness) (the-question "What hardness? ") (valid 7 8 8.5 9 10))
    (question (atr density) (the-question "What density? ") (valid 2.65 3.1 3.52 3.6 3.72 4))
    (question (atr color) (the-question "What color? ") (valid black blue brown green pink red violet white yellow colorless)))

; ===========================================================================
; RULES TO HANDLE QUESTIONING
; ===========================================================================

(defrule reload-question "Reset all the questions if user wants to continue."
    ?cont <- (continue yes)
    =>
    (do-for-all-facts ((?f question)) (eq ?f:asked yes) (modify ?f (asked no)))
    (retract ?cont))

(defrule question-text "As all text answerable questions that were not asked already and make attribute facts from the answers"
    ?f <- (question (asked no) (atr ?a) (the-question ?q) (valid $?v))
    =>
    (modify ?f (asked yes))
    (assert (attribute (name ?a) (val (ask-question ?q ?v)))))

(defrule search "Make a gem and remove answered attributes"
   ?a1 <- (attribute (name hardness) (val ?h))
   ?a2 <- (attribute (name density) (val ?d))
   ?a3 <- (attribute (name color) (val ?c))
   =>
   (assert (gem (hardness ?h) (density ?d) (color ?c)))
   (retract ?a1 ?a2 ?a3))

; ===========================================================================
;  RULES TO IDENTIFY GEMS
; ===========================================================================

(defrule indentify-gem "Rule to identify a any gem"
    ?f <- (gem 
            (name ?n&:(neq ?n nil))
            (hardness ?h)
            (density ?d)
            (color $?c))
    ?f1 <- (gem 
            (name ?n1&:(eq ?n1 nil))
            (hardness ?h1&:(eq ?h1 ?h))
            (density ?d1&:(eq ?d1 ?d))
            (color ?c1&:(member ?c1 $?c)))
    =>
    (modify ?f1 (name ?n))
    (printout t "The gem is " ?n crlf)
    (assert (success yes)))

(defrule none-matched "Rule to set failure"
    ?f <- (gem (name ?n&:(eq ?n nil)))
    =>
    (retract ?f)
    (printout t "There was no gem matched!" crlf)
    (assert (success no)))

; ===========================================================================
;  CONTROLL RULES
; ===========================================================================

(defrule continue-after-success "Ask user to continue after a gem was found"
    ?f <- (success yes)
    =>
    (retract ?f)
    (printout t "Would you like to continue identifying gems? (yes, no) " crlf)
    (bind ?a (read))
    (while (and (neq ?a yes) (neq ?a no)) do
        (printout t "The answer was invalid. Would you like to continue identifying gems? (yes, no) " crlf)
        (bind ?a (read)))
        (assert (continue ?a)))

(defrule continue-after-failure "Ask user to continue after no gem was found"
    ?f <- (success no)
    =>
    (retract ?f)
    (printout t "Would you like to continue identifying gems? (yes, no) " crlf)
    (bind ?a (read))
    (while (and (neq ?a yes) (neq ?a no)) do
        (printout t "The answer was invalid. Would you like to continue identifying gems? (yes, no) " crlf)
        (bind ?a (read)))
        (assert (continue ?a)))

(defrule continue-yes "Mar questions for reset if user wants to continue"
    ?f <- (continue yes)
    =>
    (retract ?f)
    (assert (reload-questions yes)))

(defrule continue-no "Exit the application once user does not want to continue"
    ?f <- (continue no)
    =>
    (retract ?f)
    (printout t "Goodbye." crlf)
    (halt))

; ===========================================================================
;  GEM FACTS
; ===========================================================================

(deffacts gems_table "The list of gems"
    (gem (name Diamond)(hardness 10)(density 3.52)(color yellow brown green blue white colorless))
    (gem (name Corundum)(hardness 9)(density 4)(color red pink yellow brown green blue violet black white colorless))
    (gem (name Chrysoberyl)(hardness 8.5)(density 3.72)(color yellow brown green))
    (gem (name Spinel)(hardness 8)(density 3.6)(color red pink yellow brown green blue violet white colorless))
    (gem (name Quartz)(hardness 7)(density 2.65)(color red pink green blue violet white black colorless))
    (gem (name Tourmaline)(hardness 7)(density 3.1)(color red pink yellow brown green blue white black colorless))
    )