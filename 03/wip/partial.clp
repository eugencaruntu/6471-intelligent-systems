(defrule gemOne
   (a ?)
   (b ?)
   (c ?)
   =>)

(defrule gemTwo
   (not (d ?))
   (exists (b ?x)
           (c ?x))
   =>)

(defrule loadstuff
    =>
    (assert (a 1) (b 1) (b 2) (c 1))
)
(defrule partialMatches
    =>
    (matches gemOne)
    (matches gemTwo)
)
