; ----------------------------
;  Eugen Caruntu, 29077103
; ----------------------------

; ===========================================================================
; DEFTEMPLATES
; ===========================================================================

(deftemplate attribute "Intermediate attributes used to identify a gem from user answers"
    (slot name (allowed-values hardness density color))
    (slot val)
    (slot gem (default undecided)))

(deftemplate question "The questions template related to gem attributes"
    (slot asked (default no))
    (slot atr (allowed-values hardness density color))
    (slot the-question (type STRING))
    (multislot possibilities)
    (multislot range))

; ===========================================================================
; FUNCTIONS
; ===========================================================================

(deffunction ask-question-text (?q ?v)
   (printout t ?q ?v crlf)
   (bind ?answer (read))
   (while (not (member ?answer ?v)) do
      (printout t "The value must be one of the indicated values. Please retry." crlf ?q ?v crlf)
      (bind ?answer (read)))
   ?answer)

(deffunction ask-question-ranges (?q ?lower ?upper)
   (printout t ?q "[" ?lower " - " ?upper"]" crlf)
   (bind ?answer (read))
   (while (or (not (numberp ?answer))
              (< ?answer ?lower)
              (> ?answer ?upper)) do
      (printout t "The value must be between indicated range. Please retry." crlf ?q "[" ?lower " - " ?upper"]" crlf)
      (bind ?answer (read)))
   ?answer)

(deffunction askcontinue (?s)
    (printout t crlf "Would you like to continue identifying gems? (yes, no) " ?s crlf)
        (bind ?a (read))
        (while (and (neq ?a yes) (neq ?a no)) do
            (printout t "The answer was invalid. Would you like to continue identifying gems? (yes, no) " crlf)
            (bind ?a (read)))
            (assert (continue ?a)))

; ===========================================================================
; QUESTIONS
; ===========================================================================

(deffacts questions "The questions to ask"
    (question (atr hardness) (the-question "What hardness? ") (range 1 10))
    (question (atr density) (the-question "What density? ") (range 1 6))
    (question (atr color) (the-question "What color? ") (possibilities black blue brown green pink red violet white yellow colorless)))

; ===========================================================================
; RULES TO HANDLE QUESTIONING
; ===========================================================================

(defrule question-text "As all text answerable questions that were not asked already and make attribute facts from the answers"
    ?f <- (question (asked no) (atr ?a) (the-question ?q) (possibilities $?v))
    =>
    (modify ?f (asked yes))
    (assert (attribute (name ?a) (val (ask-question-text ?q ?v)))))

(defrule question-ranges "As all number range answerable questions that were not asked already and make attribute facts from the answers"
    ?f <- (question (asked no) (atr ?a) (the-question ?q) (range ?lower ?upper))
    =>
    (modify ?f (asked yes))
    (assert (attribute (name ?a) (val (ask-question-ranges ?q ?lower ?upper)))))

; ===========================================================================
;  CONTROLL RULES
; ===========================================================================

(defrule continue-ask "Ask user to continue"
    ?f <- (cycle done)
    => (retract ?f)
    (do-for-all-facts ((?f attribute)) (or (eq ?f:name hardness) (eq ?f:name density) (eq ?f:name color)) (retract ?f))
    (do-for-all-facts ((?f question)) (eq ?f:asked yes) (modify ?f (asked no)))
    (askcontinue "?"))

(defrule continue-one-part "Go to two part matches"
    ?s <- (doing one-attr)
    ?h <- (part-hardness done)
    ?d <- (part-density done)
    ?c <- (part-color done)
    => (retract ?s ?h ?d ?c)
    (assert (doing two-attr))
    (printout t crlf))

(defrule two-parts-done "After two part identification is done"
    ?f <- (doing two-attr)
    (or ?f1 <- (hardness-density done)
        ?f1 <- (hardness-color done)
        ?f1 <- (density-color done))
    => (retract ?f ?f1)
    (assert (cycle done)))

(defrule continue-yes "Mark questions for reset if user wants to continue"
    ?f <- (continue yes)
    => (retract ?f))

(defrule continue-no "Exit the application once user does not want to continue"
    ?f <- (continue no)
    => (retract ?f)
    (printout t "Goodbye." crlf)
    (halt))

; ===========================================================================
;  GEM SPECIFIC RULES
; ===========================================================================

(defrule Diamond
    (or ?f <- (attribute (name hardness) (val ?h&:(and (>= ?h 9.9) (<= ?h 10.1))) (gem ?n&:(and (eq ?n undecided) (neq Diamond ?n))))
        ?f <- (attribute (name density) (val ?d&:(and (>= ?d 3.42) (<= ?d 3.62))) (gem ?n&:(and (eq ?n undecided) (neq Diamond ?n))))
        ?f <- (attribute (name color) (val ?c&:(member ?c (create$ yellow brown green blue white colorless))) (gem ?n&:(and (eq ?n undecided) (neq Diamond ?n)))))
    =>  (duplicate ?f (gem Diamond)))

(defrule Corundum
    (or ?f <- (attribute (name hardness) (val ?h&:(and (>= ?h 8.9) (<= ?h 9.1))) (gem ?n&:(and (eq ?n undecided) (neq Corundum ?n))))
        ?f <- (attribute (name density) (val ?d&:(and (>= ?d 3.9) (<= ?d 4.1))) (gem ?n&:(and (eq ?n undecided) (neq Corundum ?n))))
        ?f <- (attribute (name color) (val ?c&:(member ?c (create$ red pink yellow brown green blue violet black white colorless))) (gem ?n&:(and (eq ?n undecided) (neq Corundum ?n)))))
    =>  (duplicate ?f (gem Corundum)))

(defrule Chrysoberyl
    (or ?f <- (attribute (name hardness) (val ?h&:(and (>= ?h 8.4) (<= ?h 8.6))) (gem ?n&:(and (eq ?n undecided) (neq Chrysoberyl ?n))))
        ?f <- (attribute (name density) (val ?d&:(and (>= ?d 3.62) (<= ?d 3.82))) (gem ?n&:(and (eq ?n undecided) (neq Chrysoberyl ?n))))
        ?f <- (attribute (name color) (val ?c&:(member ?c (create$ yellow brown green))) (gem ?n&:(and (eq ?n undecided) (neq Chrysoberyl ?n)))))
    =>  (duplicate ?f (gem Chrysoberyl)))

(defrule Spinel
    (or ?f <- (attribute (name hardness) (val ?h&:(and (>= ?h 7.9) (<= ?h 8.1))) (gem ?n&:(and (eq ?n undecided) (neq Spinel ?n))))
        ?f <- (attribute (name density) (val ?d&:(and (>= ?d 3.5) (<= ?d 3.7))) (gem ?n&:(and (eq ?n undecided) (neq Spinel ?n))))
        ?f <- (attribute (name color) (val ?c&:(member ?c (create$ red pink yellow brown green blue violet white colorless))) (gem ?n&:(and (eq ?n undecided) (neq Spinel ?n)))))
    =>  (duplicate ?f (gem Spinel)))
    
(defrule Topaz
    (or ?f <- (attribute (name hardness) (val ?h&:(and (>= ?h 7.9) (<= ?h 8.1))) (gem ?n&:(and (eq ?n undecided) (neq Topaz ?n))))
        ?f <- (attribute (name density) (val ?d&:(and (>= ?d 3.52) (<= ?d 3.56))) (gem ?n&:(and (eq ?n undecided) (neq Topaz ?n))))
        ?f <- (attribute (name color) (val ?c&:(member ?c (create$ red pink yellow brown blue violet white colorless))) (gem ?n&:(and (eq ?n undecided) (neq Topaz ?n)))))
    =>  (duplicate ?f (gem Topaz)))

(defrule Beryl
    (or ?f <- (attribute (name hardness) (val ?h&:(and (>= ?h 7.5) (<= ?h 8.0))) (gem ?n&:(and (eq ?n undecided) (neq Beryl ?n))))
        ?f <- (attribute (name density) (val ?d&:(and (>= ?d 2.6) (<= ?d 2.8))) (gem ?n&:(and (eq ?n undecided) (neq Beryl ?n))))
        ?f <- (attribute (name color) (val ?c&:(member ?c (create$ red pink yellow brown green blue white colorless))) (gem ?n&:(and (eq ?n undecided) (neq Beryl ?n)))))
    =>  (duplicate ?f (gem Beryl)))
    
(defrule Zircon
    (or ?f <- (attribute (name hardness) (val ?h&:(and (>= ?h 6) (<= ?h 7.5))) (gem ?n&:(and (eq ?n undecided) (neq Zircon ?n))))
        ?f <- (attribute (name density) (val ?d&:(and (>= ?d 4.6) (<= ?d 4.8))) (gem ?n&:(and (eq ?n undecided) (neq Zircon ?n))))
        ?f <- (attribute (name color) (val ?c&:(member ?c (create$ yellow brown green violet white colorless))) (gem ?n&:(and (eq ?n undecided) (neq Zircon ?n)))))
    =>  (duplicate ?f (gem Zircon)))

(defrule Quartz
    (or ?f <- (attribute (name hardness) (val ?h&:(and (>= ?h 6.9) (<= ?h 7.1))) (gem ?n&:(and (eq ?n undecided) (neq Quartz ?n))))
        ?f <- (attribute (name density) (val ?d&:(and (>= ?d 2.55) (<= ?d 2.75))) (gem ?n&:(and (eq ?n undecided) (neq Quartz ?n))))
        ?f <- (attribute (name color) (val ?c&:(member ?c (create$ red pink green blue violet white black colorless))) (gem ?n&:(and (eq ?n undecided) (neq Quartz ?n)))))
    =>  (duplicate ?f (gem Quartz)))

(defrule Tourmaline
    (or ?f <- (attribute (name hardness) (val ?h&:(and (>= ?h 6.9) (<= ?h 7.1))) (gem ?n&:(and (eq ?n undecided) (neq Tourmaline ?n))))
        ?f <- (attribute (name density) (val ?d&:(and (>= ?d 3.0) (<= ?d 3.2))) (gem ?n&:(and (eq ?n undecided) (neq Tourmaline ?n))))
        ?f <- (attribute (name color) (val ?c&:(member ?c (create$ red pink yellow brown green blue white black colorless))) (gem ?n&:(and (eq ?n undecided) (neq Tourmaline ?n)))))
    =>  (duplicate ?f (gem Tourmaline)))
    
(defrule Peridot
    (or ?f <- (attribute (name hardness) (val ?h&:(and (>= ?h 6.5) (<= ?h 7))) (gem ?n&:(and (eq ?n undecided) (neq Peridot ?n))))
        ?f <- (attribute (name density) (val ?d&:(and (>= ?d 3.2) (<= ?d 3.4))) (gem ?n&:(and (eq ?n undecided) (neq Peridot ?n))))
        ?f <- (attribute (name color) (val ?c&:(member ?c (create$ yellow brown green))) (gem ?n&:(and (eq ?n undecided) (neq Peridot ?n)))))
    =>  (duplicate ?f (gem Peridot)))

(defrule Jadeite
    (or ?f <- (attribute (name hardness) (val ?h&:(and (>= ?h 6.5) (<= ?h 7))) (gem ?n&:(and (eq ?n undecided) (neq Jadeite ?n))))
        ?f <- (attribute (name density) (val ?d&:(and (>= ?d 3.2) (<= ?d 3.4))) (gem ?n&:(and (eq ?n undecided) (neq Jadeite ?n))))
        ?f <- (attribute (name color) (val ?c&:(member ?c (create$ red pink yellow brown green blue violet white black colorless))) (gem ?n&:(and (eq ?n undecided) (neq Jadeite ?n)))))
    =>  (duplicate ?f (gem Jadeite)))

(defrule Opal
    (or ?f <- (attribute (name hardness) (val ?h&:(and (>= ?h 5.5) (<= ?h 6.5))) (gem ?n&:(and (eq ?n undecided) (neq Opal ?n))))
        ?f <- (attribute (name density) (val ?d&:(and (>= ?d 2) (<= ?d 2.2))) (gem ?n&:(and (eq ?n undecided) (neq Opal ?n))))
        ?f <- (attribute (name color) (val ?c&:(member ?c (create$ red pink yellow brown white black colorless))) (gem ?n&:(and (eq ?n undecided) (neq Opal ?n)))))
    =>  (duplicate ?f (gem Opal)))

(defrule Nephrite
    (or ?f <- (attribute (name hardness) (val ?h&:(and (>= ?h 5) (<= ?h 6))) (gem ?n&:(and (eq ?n undecided) (neq Nephrite ?n))))
        ?f <- (attribute (name density) (val ?d&:(and (>= ?d 2.9) (<= ?d 3.4))) (gem ?n&:(and (eq ?n undecided) (neq Nephrite ?n))))
        ?f <- (attribute (name color) (val ?c&:(member ?c (create$ green white black colorless))) (gem ?n&:(and (eq ?n undecided) (neq Nephrite ?n)))))
    =>  (duplicate ?f (gem Nephrite)))

(defrule Turquoise
    (or ?f <- (attribute (name hardness) (val ?h&:(and (>= ?h 5) (<= ?h 6))) (gem ?n&:(and (eq ?n undecided) (neq Turquoise ?n))))
        ?f <- (attribute (name density) (val ?d&:(and (>= ?d 2.6) (<= ?d 2.8))) (gem ?n&:(and (eq ?n undecided) (neq Turquoise ?n))))
        ?f <- (attribute (name color) (val ?c&:(member ?c (create$ blue))) (gem ?n&:(and (eq ?n undecided) (neq Turquoise ?n)))))
    =>  (duplicate ?f (gem Turquoise)))
    
; ===========================================================================
;  RULES TO IDENTIFY GEMS
; ===========================================================================

(defrule identify-gem-exact "Exact identification for all characteristics"
	?f1 <- (attribute (name hardness) (gem ?g&:(neq ?g undecided)))
	?f2 <- (attribute (name density) (gem ?g&:(neq ?g undecided)))
	?f3 <- (attribute (name color) (gem ?g&:(neq ?g undecided)))
	=> (retract ?f1 ?f2 ?f3)
    (printout t "The gem is " ?g crlf)
	(assert (cycle done)))

(defrule none-matched "Rule to transition to partial matches"
    (not (cycle done))
	=> 
    (printout t crlf "No exact match!" crlf)
    (assert (doing one-attr)))

; PARTIAL IDENTIFICATION FOR ONE ATTRIBUTE

(defrule indentify-gem-hardness "Hardness identification"
    (doing one-attr)
    (not (part-hardness done))
    => (printout t crlf "  Hardness matched with: " )
    (do-for-all-facts ((?f attribute)) (and (neq ?f:gem undecided) (eq ?f:name hardness)) (printout t ?f:gem  " "))
    (assert (part-hardness done)))

(defrule indentify-gem-density "Density identification"
    (doing one-attr)
    (not (part-density done))
    => (printout t crlf "  Density matched with: ")
    (do-for-all-facts ((?f attribute)) (and (neq ?f:gem undecided) (eq ?f:name density)) (printout t ?f:gem cr " "lf))
    (assert (part-density done)))

(defrule indentify-gem-color "Color identification"
    (doing one-attr)
    (not (part-color done))
    => (printout t crlf "  Color matched with: ")
    (do-for-all-facts ((?f attribute)) (and (neq ?f:gem undecided) (eq ?f:name color)) (printout t ?f:gem " "))
    (assert (part-color done)))

; PARTIAL IDENTIFICATION FOR TWO ATTRIBUTES

(defrule identify-hardness-density "Two attributes identification"
    (doing two-attr)
    (not (hardness-density done))
	(attribute (name hardness) (gem ?g&:(neq ?g undecided)))
	(attribute (name density) (gem ?g&:(neq ?g undecided)))
	=> 
    (printout t crlf "  Best partial match is for hardness & density: " ?g crlf)
    (assert (hardness-density done)))

(defrule identify-hardness-color "Two attributes identification"
    (doing two-attr)
    (not (hardness-color done))
	(attribute (name hardness) (gem ?g&:(neq ?g undecided)))
	(attribute (name color) (gem ?g&:(neq ?g undecided)))
	=> 
    (printout t crlf "  Best partial match is for hardness & color: " ?g crlf)
    (assert (hardness-color done)))

(defrule identify-density-color "Two attributes identification"
    (doing two-attr)
    (not (density-color done))
	(attribute (name density) (gem ?g&:(neq ?g undecided)))
	(attribute (name color) (gem ?g&:(neq ?g undecided)))
	=> 
    (printout t crlf "  Best partial match is for density & color: " ?g crlf)
    (assert (density-color done)))

(defrule hardness-density-failed "Two attributes identification failed"
    (doing two-attr)
    (or (and    (not (attribute (name hardness) (gem ?g&:(neq ?g undecided))))
                (not (attribute (name density) (gem ?g&:(neq ?g undecided)))))
        (and    (attribute (name hardness) (gem ?g&:(eq ?g undecided)))
	            (attribute (name density) (gem ?g&:(eq ?g undecided)))))
	=> (assert (hardness-density failed)))

(defrule hardness-color-failed "Two attributes identification failed"
    (doing two-attr)
    (or (and    (not (attribute (name hardness) (gem ?g&:(neq ?g undecided))))
                (not (attribute (name color) (gem ?g&:(neq ?g undecided)))))
        (and    (attribute (name hardness) (gem ?g&:(eq ?g undecided)))
	            (attribute (name color) (gem ?g&:(eq ?g undecided)))))
	=> (assert (hardness-color failed)))

(defrule density-color-failed "Two attributes identification failed"
    (doing two-attr)
    (or (and    (not (attribute (name density) (gem ?g&:(neq ?g undecided))))
                (not (attribute (name color) (gem ?g&:(neq ?g undecided)))))
        (and    (attribute (name density) (gem ?g&:(eq ?g undecided)))
	            (attribute (name color) (gem ?g&:(eq ?g undecided)))))
	=> (assert (density-color failed)))   

(defrule two-part-failed
    ?f <- (doing two-attr)
    ?f1 <- (hardness-density failed)
    ?f2 <- (hardness-color failed)
    ?f3 <- (density-color failed)
    => (retract ?f ?f1 ?f2 ?f3)
    (printout t crlf "  There were no two attribute matched." crlf)
    (assert (cycle done)))