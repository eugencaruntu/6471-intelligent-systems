; ----------------------------
;  Eugen Caruntu, 29077103
; ----------------------------

; ===========================================================================
; DEFTEMPLATES
; ===========================================================================

(deftemplate gem "The gem template"
   (slot name)
   (slot hardness)
   (slot density)
   (multislot color (allowed-values black blue brown green pink red violet white yellow colorless)))

; ===========================================================================
;  RULES TO IDENTIFY GEMS
; ===========================================================================

(defrule indentify-gem "Rule to identify a any gem"
    ?f <- (gem 
            (name ?n&:(neq ?n nil))
            (hardness ?h)
            (density ?d)
            (color $?c))
    ?f1 <- (gem 
            (name ?n1&:(eq ?n1 nil))
            (hardness ?h1&:(eq ?h1 ?h))
            (density ?d1&:(eq ?d1 ?d))
            (color ?c1&:(member ?c1 $?c)))
    =>
    (printout t "The gem is " ?n crlf))
    
; ===========================================================================
;  GEM FACTS
; ===========================================================================

(deffacts gems_table "The list of gems"
   (gem (name Chrysoberyl)(hardness 8.5)(density 3.72)(color yellow brown green)))

(deffacts given-characteristics "Given 3 characteristics, we need to identify this gem"
  (gem (hardness 8.5) (density 3.72) (color green)))
  
