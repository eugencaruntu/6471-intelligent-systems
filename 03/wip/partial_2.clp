(deffacts information
    (match a c e g)
    (item a)
    (item b)
    (item c)
    (item d)
    (item e)
    (item f)
    (item g))

(defrule match-1
    (match ?x ?y ?z ?w)
    (item ?x)
    (item ?y)
    (item ?z)
    (item ?w)
  =>
    (assert (found-match ?x ?y ?z ?w)))

(defrule match-2
    (item ?x)
    (item ?y)
    (item ?z)
    (item ?w)
    (match ?x ?y ?z ?w)
  =>
    (assert (found-match ?x ?y ?z ?w)))