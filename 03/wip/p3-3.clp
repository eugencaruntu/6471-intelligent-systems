; ----------------------------
;  Eugen Caruntu 29077103
; ----------------------------

; ===========================================================================
; DEFTEMPLATES
; ===========================================================================

(deftemplate attribute "Intermediate attributes used to build a gem from user answers"
    (slot name (allowed-values hardness density color))
    (slot val))

(deftemplate gem "The gem template"
   (slot name)
   (multislot hardness)
   (multislot density)
   (multislot color (allowed-values black blue brown green pink red violet white yellow colorless)))

(deftemplate question "The questions template related to gem attributes"
    (slot asked (default no))
    (slot atr (allowed-values hardness density color))
    (slot the-question (type STRING))
    (multislot possibilities)
    (multislot range))

; ===========================================================================
; QUESTION FACTS
; ===========================================================================

(deffacts questions "The questions to ask"
    (question (atr hardness) (the-question "What hardness? ") (range 1 10))
    (question (atr density) (the-question "What density? ") (range 1 6))
    (question (atr color) (the-question "What color? ") (possibilities black blue brown green pink red violet white yellow colorless)))

; ===========================================================================
; FUNCTIONS
; ===========================================================================

(deffunction ask-question-text (?q ?v)
   (printout t ?q ?v crlf)
   (bind ?answer (read))
   (while (not (member ?answer ?v)) do
      (printout t "The value must be one of the indicated values. Please retry." crlf ?q ?v crlf)
      (bind ?answer (read)))
   ?answer)

(deffunction ask-question-ranges (?q ?lower ?upper)
   (printout t ?q "[" ?lower " - " ?upper"]" crlf)
   (bind ?answer (read))
   (while (or (not (numberp ?answer))
              (< ?answer ?lower)
              (> ?answer ?upper)) do
      (printout t "The value must be between indicated range. Please retry." crlf ?q "[" ?lower " - " ?upper"]" crlf)
      (bind ?answer (read)))
   ?answer)

(deffunction askcontinue (?s)
    (printout t "Would you like to continue identifying gems? (yes, no) " ?s crlf)
        (bind ?a (read))
        (while (and (neq ?a yes) (neq ?a no)) do
            (printout t "The answer was invalid. Would you like to continue identifying gems? (yes, no) " crlf)
            (bind ?a (read)))
            (assert (continue ?a)))

; ===========================================================================
; RULES TO HANDLE QUESTIONING
; ===========================================================================

(defrule reload-question "Reset all the questions if user wants to continue."
    ?rel <- (reload-questions yes)
    =>
    (do-for-all-facts ((?f question)) (eq ?f:asked yes) (modify ?f (asked no)))
    (retract ?rel))

(defrule question-text "As all text answerable questions that were not asked already and make attribute facts from the answers"
    ?f <- (question (asked no) (atr ?a) (the-question ?q) (possibilities $?v))
    =>
    (modify ?f (asked yes))
    (assert (attribute (name ?a) (val (ask-question-text ?q ?v)))))

(defrule question-ranges "As all number range answerable questions that were not asked already and make attribute facts from the answers"
    ?f <- (question (asked no) (atr ?a) (the-question ?q) (range ?lower ?upper))
    =>
    (modify ?f (asked yes))
    (assert (attribute (name ?a) (val (ask-question-ranges ?q ?lower ?upper)))))

(defrule search "Make a gem and remove answered attributes"
   ?a1 <- (attribute (name hardness) (val ?h))
   ?a2 <- (attribute (name density) (val ?d))
   ?a3 <- (attribute (name color) (val ?c))
   =>
   (assert (gem (hardness ?h) (density ?d) (color ?c)))
   (retract ?a1 ?a2 ?a3))

; ===========================================================================
;  RULES TO IDENTIFY GEMS
; ===========================================================================

(defrule indentify-gem "Rule to identify a any gem"
    ?f <- (gem 
            (name ?n&:(neq ?n nil))
            (hardness ?hlower - ?hupper)
            (density ?dlower - ?dupper)
            (color $?c))
    ?f1 <- (gem 
            (name ?n1&:(eq ?n1 nil))
            (hardness ?h1&:(and (>= ?h1 ?hlower) (<= ?h1 ?hupper)))
            (density ?d1&:(and (>= ?d1 ?dlower) (<= ?d1 ?dupper)))
            (color ?c1&:(member ?c1 $?c)))
    =>
    (modify ?f1 (name ?n))
    (printout t "The gem is " ?n crlf)
    (assert (success yes)))

(defrule none-matched "Rule to set failure"
    ?f <- (gem (name ?n&:(eq ?n nil)))
    =>
    (retract ?f)
    (printout t "There was no gem matched!" crlf)
    (assert (success no)))
    
; ===========================================================================
;  CONTROLL RULES
; ===========================================================================

(defrule continue-ask "Ask user to continue"
    ?f <- (success yes|no)
    =>
    (retract ?f)
    (askcontinue "?"))

(defrule continue-yes "Mark questions for reset if user wants to continue"
    ?f <- (continue yes)
    =>
    (retract ?f)
    (assert (reload-questions yes)))

(defrule continue-no "Exit the application once user does not want to continue"
    ?f <- (continue no)
    =>
    (retract ?f)
    (printout t "Goodbye." crlf)
    (assert (reload-questions yes))
    (halt))

; ===========================================================================
;  GEM FACTS
; ===========================================================================

(deffacts gems_table "The list of gems"
    (gem (name Diamond)(hardness 9.9 - 10.1)(density 3.42 - 3.62)(color yellow brown green blue white colorless))
    (gem (name Corundum)(hardness 8.9 - 9.1)(density 3.9 - 4.1)(color red pink yellow brown green blue violet black white colorless))
    (gem (name Chrysoberyl)(hardness 8.4 - 8.6)(density 3.62 - 3.82)(color yellow brown green))
    (gem (name Spinel)(hardness 7.9 - 8.1)(density 3.5 - 3.7)(color red pink yellow brown green blue violet white colorless))
    (gem (name Topaz)(hardness 7.9 - 8.1)(density 3.52 - 3.56)(color red pink yellow brown blue violet white colorless))
    (gem (name Beryl)(hardness 7.5 - 8.0)(density 2.6 - 2.8)(color red pink yellow brown green blue white colorless))
    (gem (name Zircon)(hardness 6 - 7.5)(density 4.6 - 4.8)(color yellow brown green violet white colorless))
    (gem (name Quartz)(hardness 6.9 - 7.1)(density 2.55 - 2.75)(color red pink green blue violet white black colorless))
    (gem (name Tourmaline)(hardness 6.9 - 7.1)(density 3.0 - 3.2)(color red pink yellow brown green blue white black colorless))
    (gem (name Peridot)(hardness 6.5 - 7)(density 3.2 - 3.4)(color yellow brown green))
    (gem (name Jadeite)(hardness 6.5 - 7)(density 3.2 - 3.4)(color red pink yellow brown green blue violet white black colorless))
    (gem (name Opal)(hardness 5.5 - 6.5)(density 2 - 2.2)(color red pink yellow brown white black colorless))
    (gem (name Nephrite)(hardness 5 - 6)(density 2.9 - 3.4)(color green white black colorless))
    (gem (name Turquoise)(hardness 5 - 6)(density 2.6 - 2.8)(color blue)))
