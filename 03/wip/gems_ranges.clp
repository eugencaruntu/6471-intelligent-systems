


(deftemplate gem "The gem template"
   (slot name)
   (multislot hardness (range 1 10))
   (multislot density (range 1 6))
   (multislot color (allowed-values black blue brown green pink red violet white yellow colorless)))

(deffacts gems_table "The list of gems"
    (gem (name Diamond)(hardness 9.9 - 10.1)(density 3.42 - 3.62)(color yellow brown green blue white colorless))
    
(deffacts given-characteristics "Given 3 characteristics, we need to identify the gem"
  (gem 
       (hardness 8.5)
       (density 3.72)
       (color green)))

(defrule match-gem "Rule to identify a any gem"
    ?f0 <- (gem 
            (name ?n0&:(neq ?n0 nil))
            (hardness (range ?hlower ?hupper))
            (density (range ?dlower ?dupper))
            (color $?c0))
    ?f1 <- (gem 
            (name ?n1&:(eq ?n1 nil))
            (hardness ?h1&:(and (< ?h1 ?hlower) (> ?h1 ?hupper)))
            (density ?d1&:(and (< ?d1 ?dlower) (> ?d1 ?dupper)))
            (color ?c1&:(member ?c1 $?c0)))
    =>
    (printout t "The gem is " ?n0 crlf))
