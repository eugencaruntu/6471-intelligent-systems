; ----------------------------
;  Eugen Caruntu, 29077103
; ----------------------------

; ===========================================================================
;  RULES TO IDENTIFY GEMS
; ===========================================================================

; ------------ Chrysoberyl ------------ ;

(defrule Chrysoberyl
	?f1 <- (hardness 8.5)
    ?f2 <- (density 3.72)
    ?f3 <- (color ?c&:(member ?c (create$ yellow brown green)))
    => (retract ?f1 ?f2 ?f3)
    (printout t "The gem is Chrysoberyl" crlf)
	(halt))
   
; ===========================================================================
;  GIVEN ATTRIBUTES
; ===========================================================================

(defrule  given-attributes "Given 3 characteristics, we need to identify this gem"
	?f <- (initial-fact)
	=> (retract ?f)
  	(assert (hardness 8.5))
	(assert (density 3.72))
  	(assert (color green)))
