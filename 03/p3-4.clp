; ----------------------------
;  Eugen Caruntu, 29077103
; ----------------------------

; ===========================================================================
; FUNCTIONS
; ===========================================================================

(deffunction ask-question-text (?q ?v)
   (printout t ?q ?v crlf)
   (bind ?answer (read))
   (while (not (member ?answer ?v)) do
      (printout t "The value must be one of the indicated values. Please retry." crlf ?q ?v crlf)
      (bind ?answer (read)))
   ?answer)

(deffunction ask-question-ranges (?q ?lower ?upper)
   (printout t ?q "[" ?lower " - " ?upper"]" crlf)
   (bind ?answer (read))
   (while (or (not (numberp ?answer))
              (< ?answer ?lower)
              (> ?answer ?upper)) do
      (printout t "The value must be between indicated range. Please retry." crlf ?q "[" ?lower " - " ?upper"]" crlf)
      (bind ?answer (read)))
   ?answer)

(deffunction askcontinue (?s)
    (printout t crlf "=====================================================" crlf)
    (printout t "Would you like to continue identifying gems"?s " [yes, no]"  crlf)
        (bind ?a (read))
        (while (and (neq ?a yes) (neq ?a no)) do
            (printout t "The answer was invalid. Would you like to continue identifying gems"?s " [yes, no]" crlf)
            (bind ?a (read)))
        (assert (continue ?a)))

(deffunction more (?g)
    (printout t crlf "What facts would you like to know about "  ?g " [composition, month, none]"  crlf)
        (bind ?a (read))
        (while (and (neq ?a composition) (neq ?a month)  (neq ?a none)) do
            (printout t "The answer was invalid. What facts would you like to know about " ?g " [composition, month, none]" crlf)
            (bind ?a (read)))
    (assert (topic ?a ?g)))
    

; ===========================================================================
; QUESTIONS
; ===========================================================================

(defrule welcome "The questions to ask"
    ?f <- (initial-fact)
	=> (retract ?f)
    (printout t "=========================================" crlf)
    (printout t " This Expert System can help you with:" 
                    crlf tab "Gem Identification" 
                    crlf tab "Chemical Composition"
                    crlf tab "Birthstone Designation" crlf)
    (printout t "=========================================" crlf)
    (assert (reload questions)))

 (defrule reload-questions
    ?f <- (reload questions)
    => (retract ?f)
    (assert (question color "What color? " txt (create$ black blue brown green pink red violet white yellow colorless)))
    (assert (question density "What density? " rng 1 6))
    (assert (question hardness "What hardness? " rng 1 10))
    (set-strategy simplicity))

; ===========================================================================
; RULES TO HANDLE QUESTIONING
; ===========================================================================

(defrule question-hardness "As all number range answerable questions that were not asked already and make attribute facts from the answers"
    ?f <- (question hardness ?q rng ?lower ?upper)
    => (retract ?f)
    (assert (attribute hardness (ask-question-ranges ?q ?lower ?upper) undecided 0.5)))

(defrule question-density "As all number range answerable questions that were not asked already and make attribute facts from the answers"
    ?f <- (question density ?q rng ?lower ?upper)
    => (retract ?f)
    (assert (attribute density (ask-question-ranges ?q ?lower ?upper) undecided 0.3)))

(defrule question-color "As all text answerable questions that were not asked already and make attribute facts from the answers"
    ?f <- (question color ?q txt $?v)
    => (retract ?f)
    (assert (attribute color (ask-question-text ?q ?v) undecided 0.2)))

; ===========================================================================
;  CONTROLL RULES
; ===========================================================================

(defrule continue-ask "Ask user to continue"
    ?f <- (cycle done)
    => (retract ?f)
    (do-for-all-facts ((?f attribute)) TRUE (retract ?f))
    (do-for-all-facts ((?f topic)) TRUE (retract ?f))
    (askcontinue "?"))

(defrule continue-yes "Mark questions for reset if user wants to continue"
    ?f <- (continue yes)
    => (retract ?f)
    (assert (reload questions)))

(defrule continue-no "Exit the application once user does not want to continue"
    ?f <- (continue no)
    => (retract ?f)
    (printout t crlf "Goodbye." crlf)
    (halt))

; ===========================================================================
;  GEM SPECIFIC RULES
; ===========================================================================

(defrule Diamond
    (or ?f <- (attribute ?a&:(eq ?a hardness) ?v&:(and (>= ?v 9.9) (<= ?v 10.1)) ?n&:(and (eq ?n undecided) (neq Diamond ?n)) ?r)
        ?f <- (attribute ?a&:(eq ?a density) ?v&:(and (>= ?v 3.42) (<= ?v 3.62)) ?n&:(and (eq ?n undecided) (neq Diamond ?n)) ?r)
        ?f <- (attribute ?a&:(eq ?a color) ?v&:(member ?v (create$ yellow brown green blue white colorless)) ?n&:(and (eq ?n undecided) (neq Diamond ?n)) ?r))
    =>  (assert (attribute ?a ?v Diamond ?r)))

 (defrule Corundum
    (or ?f <- (attribute ?a&:(eq ?a hardness) ?v&:(and (>= ?v 8.9) (<= ?v 9.1)) ?n&:(and (eq ?n undecided) (neq Corundum ?n)) ?r)
        ?f <- (attribute ?a&:(eq ?a density) ?v&:(and (>= ?v 3.9) (<= ?v 4.1)) ?n&:(and (eq ?n undecided) (neq Corundum ?n)) ?r)
        ?f <- (attribute ?a&:(eq ?a color) ?v&:(member ?v (create$ red pink yellow brown green blue violet black white colorless)) ?n&:(and (eq ?n undecided) (neq Corundum ?n)) ?r))
    =>  (assert (attribute ?a ?v Corundum ?r)))

(defrule Chrysoberyl
    (or ?f <- (attribute ?a&:(eq ?a hardness) ?v&:(and (>= ?v 8.4) (<= ?v 8.6)) ?n&:(and (eq ?n undecided) (neq Chrysoberyl ?n)) ?r)
        ?f <- (attribute ?a&:(eq ?a density) ?v&:(and (>= ?v 3.62) (<= ?v 3.82)) ?n&:(and (eq ?n undecided) (neq Chrysoberyl ?n)) ?r)
        ?f <- (attribute ?a&:(eq ?a color) ?v&:(member ?v (create$ yellow brown green)) ?n&:(and (eq ?n undecided) (neq Chrysoberyl ?n)) ?r))
    =>  (assert (attribute ?a ?v Chrysoberyl ?r)))

(defrule Spinel
    (or ?f <- (attribute ?a&:(eq ?a hardness) ?v&:(and (>= ?v 7.9) (<= ?v 8.1)) ?n&:(and (eq ?n undecided) (neq Spinel ?n)) ?r)
        ?f <- (attribute ?a&:(eq ?a density) ?v&:(and (>= ?v 3.5) (<= ?v 3.7)) ?n&:(and (eq ?n undecided) (neq Spinel ?n)) ?r)
        ?f <- (attribute ?a&:(eq ?a color) ?v&:(member ?v (create$ red pink yellow brown green blue violet white colorless)) ?n&:(and (eq ?n undecided) (neq Spinel ?n)) ?r))
    =>  (assert (attribute ?a ?v Spinel ?r)))
    
(defrule Topaz
    (or ?f <- (attribute ?a&:(eq ?a hardness) ?v&:(and (>= ?v 7.9) (<= ?v 8.1)) ?n&:(and (eq ?n undecided) (neq Topaz ?n)) ?r)
        ?f <- (attribute ?a&:(eq ?a density) ?v&:(and (>= ?v 3.52) (<= ?v 3.56)) ?n&:(and (eq ?n undecided) (neq Topaz ?n)) ?r)
        ?f <- (attribute ?a&:(eq ?a color) ?v&:(member ?v (create$ red pink yellow brown blue violet white colorless)) ?n&:(and (eq ?n undecided) (neq Topaz ?n)) ?r))
    =>  (assert (attribute ?a ?v Topaz ?r)))

(defrule Beryl
    (or ?f <- (attribute ?a&:(eq ?a hardness) ?v&:(and (>= ?v 7.5) (<= ?v 8.0)) ?n&:(and (eq ?n undecided) (neq Beryl ?n)) ?r)
        ?f <- (attribute ?a&:(eq ?a density) ?v&:(and (>= ?v 2.6) (<= ?v 2.8)) ?n&:(and (eq ?n undecided) (neq Beryl ?n)) ?r)
        ?f <- (attribute ?a&:(eq ?a color) ?v&:(member ?v (create$ red pink yellow brown green blue white colorless)) ?n&:(and (eq ?n undecided) (neq Beryl ?n)) ?r))
    =>  (assert (attribute ?a ?v Beryl ?r)))
    
(defrule Zircon
    (or ?f <- (attribute ?a&:(eq ?a hardness) ?v&:(and (>= ?v 6) (<= ?v 7.5)) ?n&:(and (eq ?n undecided) (neq Zircon ?n)) ?r)
        ?f <- (attribute ?a&:(eq ?a density) ?v&:(and (>= ?v 4.6) (<= ?v 4.8)) ?n&:(and (eq ?n undecided) (neq Zircon ?n)) ?r)
        ?f <- (attribute ?a&:(eq ?a color) ?v&:(member ?v (create$ yellow brown green violet white colorless)) ?n&:(and (eq ?n undecided) (neq Zircon ?n)) ?r))
    =>  (assert (attribute ?a ?v Zircon ?r)))

(defrule Quartz
    (or ?f <- (attribute ?a&:(eq ?a hardness) ?v&:(and (>= ?v 6.9) (<= ?v 7.1)) ?n&:(and (eq ?n undecided) (neq Quartz ?n)) ?r)
        ?f <- (attribute ?a&:(eq ?a density) ?v&:(and (>= ?v 2.55) (<= ?v 2.75)) ?n&:(and (eq ?n undecided) (neq Quartz ?n)) ?r)
        ?f <- (attribute ?a&:(eq ?a color) ?v&:(member ?v (create$ red pink green blue violet white black colorless)) ?n&:(and (eq ?n undecided) (neq Quartz ?n)) ?r))
    =>  (assert (attribute ?a ?v Quartz ?r)))

(defrule Tourmaline
    (or ?f <- (attribute ?a&:(eq ?a hardness) ?v&:(and (>= ?v 6.9) (<= ?v 7.1)) ?n&:(and (eq ?n undecided) (neq Tourmaline ?n)) ?r)
        ?f <- (attribute ?a&:(eq ?a density) ?v&:(and (>= ?v 3.0) (<= ?v 3.2)) ?n&:(and (eq ?n undecided) (neq Tourmaline ?n)) ?r)
        ?f <- (attribute ?a&:(eq ?a color) ?v&:(member ?v (create$ red pink yellow brown green blue white black colorless)) ?n&:(and (eq ?n undecided) (neq Tourmaline ?n)) ?r))
    =>  (assert (attribute ?a ?v Tourmaline ?r)))
    
(defrule Peridot
    (or ?f <- (attribute ?a&:(eq ?a hardness) ?v&:(and (>= ?v 6.5) (<= ?v 7)) ?n&:(and (eq ?n undecided) (neq Peridot ?n)) ?r)
        ?f <- (attribute ?a&:(eq ?a density) ?v&:(and (>= ?v 3.2) (<= ?v 3.4)) ?n&:(and (eq ?n undecided) (neq Peridot ?n)) ?r)
        ?f <- (attribute ?a&:(eq ?a color) ?v&:(member ?v (create$ yellow brown green)) ?n&:(and (eq ?n undecided) (neq Peridot ?n)) ?r))
    =>  (assert (attribute ?a ?v Peridot ?r)))

(defrule Jadeite
    (or ?f <- (attribute ?a&:(eq ?a hardness) ?v&:(and (>= ?v 6.5) (<= ?v 7)) ?n&:(and (eq ?n undecided) (neq Jadeite ?n)) ?r)
        ?f <- (attribute ?a&:(eq ?a density) ?v&:(and (>= ?v 3.2) (<= ?v 3.4)) ?n&:(and (eq ?n undecided) (neq Jadeite ?n)) ?r)
        ?f <- (attribute ?a&:(eq ?a color) ?v&:(member ?v (create$ red pink yellow brown green blue violet white black colorless)) ?n&:(and (eq ?n undecided) (neq Jadeite ?n)) ?r))
    =>  (assert (attribute ?a ?v Jadeite ?r)))

(defrule Opal
    (or ?f <- (attribute ?a&:(eq ?a hardness) ?v&:(and (>= ?v 5.5) (<= ?v 6.5)) ?n&:(and (eq ?n undecided) (neq Opal ?n)) ?r)
        ?f <- (attribute ?a&:(eq ?a density) ?v&:(and (>= ?v 2) (<= ?v 2.2)) ?n&:(and (eq ?n undecided) (neq Opal ?n)) ?r)
        ?f <- (attribute ?a&:(eq ?a color) ?v&:(member ?v (create$ red pink yellow brown white black colorless)) ?n&:(and (eq ?n undecided) (neq Opal ?n)) ?r))
    =>  (assert (attribute ?a ?v Opal ?r)))

(defrule Nephrite
    (or ?f <- (attribute ?a&:(eq ?a hardness) ?v&:(and (>= ?v 5) (<= ?v 6)) ?n&:(and (eq ?n undecided) (neq Nephrite ?n)) ?r)
        ?f <- (attribute ?a&:(eq ?a density) ?v&:(and (>= ?v 2.9) (<= ?v 3.4)) ?n&:(and (eq ?n undecided) (neq Nephrite ?n)) ?r)
        ?f <- (attribute ?a&:(eq ?a color) ?v&:(member ?v (create$ green white black colorless)) ?n&:(and (eq ?n undecided) (neq Nephrite ?n)) ?r))
    =>  (assert (attribute ?a ?v Nephrite ?r)))  

(defrule Turquoise
    (or ?f <- (attribute ?a&:(eq ?a hardness) ?v&:(and (>= ?v 5) (<= ?v 6)) ?n&:(and (eq ?n undecided) (neq Turquoise ?n)) ?r)
        ?f <- (attribute ?a&:(eq ?a density) ?v&:(and (>= ?v 2.6) (<= ?v 2.8)) ?n&:(and (eq ?n undecided) (neq Turquoise ?n)) ?r)
        ?f <- (attribute ?a&:(eq ?a color) ?v&:(member ?v (create$ blue)) ?n&:(and (eq ?n undecided) (neq Turquoise ?n)) ?r))
    =>  (assert (attribute ?a ?v Turquoise ?r)))
    
; ===========================================================================
;  RULES TO IDENTIFY GEMS
; ===========================================================================

(defrule identify-gem-exact "Exact identification for all characteristics"
	?f1 <- (attribute hardness ?v1 ?g&:(neq ?g undecided) ?r1)
	?f2 <- (attribute density ?v2 ?g&:(neq ?g undecided) ?r2)
	?f3 <- (attribute color ?v3 ?g&:(neq ?g undecided) ?r3)
	=> (retract ?f1 ?f2 ?f3)
    (printout t crlf "Exact match: " tab ?g crlf)
    (more ?g)
    (assert (exact)))

(defrule allow-multiple-exact-topics "Set strategy to breadth to allow for any multiple exact matches"
    ?f0 <- (exact)
    (not (cycle done))
    ?f1 <- (attribute hardness ?v1 ?g&:(eq ?g undecided) ?r1)
	?f2 <- (attribute density ?v2 ?g&:(eq ?g undecided) ?r2)
	?f3 <- (attribute color ?v3 ?g&:(eq ?g undecided) ?r3)
	=> (retract ?f0 ?f1 ?f2 ?f3)
    (set-strategy breadth)
    (assert (cycle done)))

(defrule topics-none "Rule to skip when user does not want additional topics"
    ?f <- (topic none)
	=> (retract ?f)
    (assert (cycle done)))

(defrule none-matched "Rule to transition to partial matches once no exact match found"
    (not (question))
    (not (topic))
    (not (cycle done))
    ?f1 <- (attribute hardness ?v1 ?g&:(eq ?g undecided) ?r1)
	?f2 <- (attribute density ?v2 ?g&:(eq ?g undecided) ?r2)
	?f3 <- (attribute color ?v3 ?g&:(eq ?g undecided) ?r3)
	=> (retract ?f1 ?f2 ?f3)
    (printout t crlf "No exact match!" crlf)
    (set-strategy depth)
    (assert (doing partial)))

; PARTIAL IDENTIFICATION FOR ONE ATTRIBUTE

(defrule indentify-gem-hardness "Hardness identification"
    (doing partial)
    (attribute hardness ?v ?g ?r)
    => (printout t crlf "Hardness matched with: " ?g tab)
)

(defrule indentify-gem-density "Density identification"
    (doing partial)
    (attribute density ?v ?g ?r)
    => (printout t crlf "Density matched with: " ?g tab)
)

(defrule indentify-gem-color "Color identification"
    (doing partial)
    (attribute color ?v ?g ?r)
    => (printout t crlf "Color matched with: " ?g tab)
    )

; PARTIAL IDENTIFICATION FOR TWO ATTRIBUTES

(defrule identify-two-attributes "Two attributes identification"
    (doing partial)
    (or 
        (and    ?f1 <- (attribute ?a1&:(eq ?a1 hardness) ?v1 ?g ?r1)
                ?f2 <- (attribute ?a2&:(eq ?a2 density) ?v2 ?g ?r2))
        (and    ?f1 <- (attribute ?a1&:(eq ?a1 hardness) ?v1 ?g ?r1)
                ?f2 <- (attribute ?a2&:(eq ?a2 color) ?v2 ?g ?r2))
        (and    ?f1 <- (attribute ?a1&:(eq ?a1 density) ?v1 ?g ?r1)
                ?f2 <- (attribute ?a2&:(eq ?a2 color) ?v2 ?g ?r2))
    )
	=> (retract ?f1 ?f2)
    (printout t crlf "Partial match: " tab ?g tab "[" (+ ?r1 ?r2) " certainty because " ?a1 " (" ?r1 ") & " ?a2 " (" ?r2 ") matched]" ))

(defrule done-two-attributes "Two attributes identification is done"
    ?f <- (doing partial)
    (not (and   (attribute ?a1&:(eq ?a1 hardness) ?v1 ?g ?r1)
                (attribute ?a2&:(eq ?a2 density) ?v2 ?g ?r2)))
    (not (and   (attribute ?a1&:(eq ?a1 hardness) ?v1 ?g ?r1)
                (attribute ?a2&:(eq ?a2 color) ?v2 ?g ?r2)))
    (not (and   (attribute ?a1&:(eq ?a1 density) ?v1 ?g ?r1)
                (attribute ?a2&:(eq ?a2 color) ?v2 ?g ?r2)))
	=> (retract ?f)
   (assert (cycle done)))

; ===========================================================================
;  GEM COMPOSITION RULES
; ===========================================================================

(defrule Diamond-composition
    ?f <- (topic composition Diamond)
    =>  (retract ?f)
    (printout t tab "Components: carbon" crlf))

(defrule Corundum-composition
    ?f <- (topic composition Corundum)
    =>  (retract ?f)
    (printout t tab "Components: aluminium oxide" crlf)
    )

(defrule Chrysoberyl-composition
    ?f <- (topic composition Chrysoberyl)
    =>  (retract ?f)
    (printout t tab "Components: aluminate of beryllium" crlf))

(defrule Spinel-composition
    ?f <- (topic composition Spinel)
    =>  (retract ?f)
    (printout t tab "Components: magnesium aluminium" crlf))

(defrule Topaz-composition
    ?f <- (topic composition Topaz)
    =>  (retract ?f)
    (printout t tab "Components: silicate of aluminium and fluorine" crlf))

(defrule Beryl-composition
    ?f <- (topic composition Beryl)
    =>  (retract ?f)
    (printout t tab "Components: beryllium aluminium cyclosilicate" crlf))

(defrule Zircon-composition
    ?f <- (topic composition Zircon)
    =>  (retract ?f)
    (printout t tab "Components: zirconium silicate" crlf))

(defrule Quartz-composition
    ?f <- (topic composition Quartz)
    =>  (retract ?f)
    (printout t tab "Components: silicon and oxygen" crlf))

(defrule Tourmaline-composition
    ?f <- (topic composition Tourmaline)
    =>  (retract ?f)
    (printout t tab "Components: crystalline boron silicate compounded with aluminium, iron, magnesium, sodium, lithium, or potassium" crlf))

(defrule Peridot-composition
    ?f <- (topic composition Peridot)
    =>  (retract ?f)
    (printout t tab "Components: magnesium rich olivine and silicate" crlf))

(defrule Jadeite-composition
    ?f <- (topic composition Jadeite)
    =>  (retract ?f)
    (printout t tab "Components: pyroxene composed of sodium, aluminium, silicate and oxygen" crlf))

(defrule Opal-composition
    ?f <- (topic composition Opal)
    =>  (retract ?f)
    (printout t tab "Components: hydrated amorphous silica" crlf))

(defrule Nephrite-composition
    ?f <- (topic composition Nephrite)
    =>  (retract ?f)
    (printout t tab "Components: calcium, magnesium, iron, silica, oxygen" crlf))

(defrule Turquoise-composition
    ?f <- (topic composition Turquoise)
    =>  (retract ?f)
    (printout t tab "Components: hydrated phosphate of copper and aluminium" crlf))

; ===========================================================================
;  GEM MONTH RULES
; ===========================================================================

(defrule Diamond-month
    ?f <- (topic month Diamond)
    =>  (retract ?f)
    (printout t tab "Birthstone: April" crlf))

(defrule Corundum-month
    ?f <- (topic month Corundum)
    =>  (retract ?f)
    (printout t tab "Birthstone: July (Ruby), September (Sapphire)" crlf))

(defrule Chrysoberyl-month
    ?f <- (topic month Chrysoberyl)
    =>  (retract ?f)
    (printout t tab "Birthstone: June (Alexandrite)" crlf))

(defrule Spinel-month
    ?f <- (topic month Spinel)
    =>  (retract ?f)
    (printout t tab "Birthstone: August" crlf))

(defrule Topaz-month
    ?f <- (topic month Topaz)
    =>  (retract ?f)
    (printout t tab "Birthstone: November" crlf))

(defrule Beryl-month
    ?f <- (topic month Beryl)
    =>  (retract ?f)
    (printout t tab "Birthstone: March (Aquamarine)" crlf))

(defrule Zircon-month
    ?f <- (topic month Zircon)
    =>  (retract ?f)
    (printout t tab "Birthstone: December" crlf))

(defrule Quartz-month
    ?f <- (topic month Quartz)
    =>  (retract ?f)
    (printout t tab "Birthstone: April" crlf))

(defrule Tourmaline-month
    ?f <- (topic month Tourmaline)
    =>  (retract ?f)
    (printout t tab "Birthstone: October" crlf))

(defrule Peridot-month
    ?f <- (topic month Peridot)
    =>  (retract ?f)
    (printout t tab "Birthstone: April" crlf))

(defrule Jadeite-month
    ?f <- (topic month Jadeite)
    =>  (retract ?f)
    (printout t tab "Birthstone: February (Amethyst)" crlf))

(defrule Opal-month
    ?f <- (topic month Opal)
    =>  (retract ?f)
    (printout t tab "Birthstone: October" crlf))

(defrule Nephrite-month
    ?f <- (topic month Nephrite)
    =>  (retract ?f)
    (printout t tab "Birthstone: September" crlf))

(defrule Turquoise-month
    ?f <- (topic month Turquoise)
    =>  (retract ?f)
    (printout t tab "Birthstone: December" crlf))
