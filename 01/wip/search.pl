
% move/2 and goal/1 are specific to blocks puzzle
move(State, NextState):-
	select([Top|Stack1], State, Rest), 			% selects the first list from the current state
	select(Stack2, Rest, OtherStacks),			% selects the first list from the rest of the lists of current state
	NextState = [Stack1,[Top|Stack2]|OtherStacks].

% avoid cycles
move_cyclefree(Visited, Node, NextNode):-
		move(Node, NextNode),
		\+ member(NextNode, Visited).
		
goal(State):-
	member([a,b,c], State).

% == Bounded DFS ================================
% solve_dfs(3, [[c,b,a],[],[]], Plan).	
% solve_dfs(3, [[c,a],[b],[]], Plan).

solve_dfs(Bound, Node, Path):-
	dfs(Bound, [Node], Node, RevPath),
	reverse(RevPath, Path).
	
dfs(_, Visited, Node, Visited):-
	goal(Node).

dfs(Bound, Visited, Node, Path):-
	Bound > 0,
	move_cyclefree(Visited, Node, NextNode),
	NewBound is Bound - 1,
	dfs(NewBound, [NextNode|Visited], NextNode, Path).

% == BFS ========================================
% solve_bfs([[c,a],[b],[]], Plan).

solve_bfs(Node, Path):-
	bfs([[Node]], RevPath),
	reverse(RevPath, Path).

bfs([[Node|Path]|_], [Node|Path]):-
	goal(Node).
	
bfs([Path|Paths], SolutionPath):-
	expand_bfs(Path, ExpPaths),
	append(Paths, ExpPaths, NewPaths),
	bfs(NewPaths, SolutionPath).

expand_bfs([Node|Path], ExpPaths):-
	findall([NewNode, Node|Path],
			move_cyclefree(Path, Node, NewNode),
			ExpPaths).
			
			
			
% == A* ==========================================
	

