% ===========================================================================
% NUMBERLINK PUZZLE SOLVER
% Eugen Caruntu, 29077103
% ===========================================================================

% play(+File, -File)
play(GameFile, SolutionFile):-
	read_file(GameFile, InputList),
	convert_input(InputList, N, K, PairsList),
	empty_board(N, Board),
	fill_cells(PairsList, Board),
	move([PairsList, Board, []], [_, FinalBoard, SolPath]), 
	assemble_solution(SolPath, N, K, FinalSolution),
	write_file(SolutionFile, FinalSolution),
	% TODO: move this to DFS, BFS, As once implemented
	goal([_, FinalBoard, SolPath]), % determines if there are no free atoms left on the board
	!.

% ===========================================================================
% CONSTRAINTS
% ===========================================================================

% Constraint 1: allowed 'moves' only up/down or left/right
% adjacent(+Cell1, +Cell2)
adjacent((X,Y1), (X,Y)) :- Y is Y1 +1; Y is Y1 -1.
adjacent((X1,Y), (X,Y)) :- X is X1 +1; X is X1 -1.

%  Constraint 2: two cells can be connected if they have the same number:
% 	- get the Yth sublist within the Board
% 	- the Xth cell on the sublist that has the same Nbr as the cell being connected to
% connect(+Cell1, ?Cell2, ?Board)
connect((X,Y), (X1,Y1), Board):-
    nth1(Y, Board, Sublist),
    nth1(X, Sublist, Nbr),
    nth1(Y1, Board, Sublist1),
    nth1(X1, Sublist1, Nbr).

%  A move is valid if:
% 	- the connection is done with an adjacent cell: adjacent/2
% 	- the cell being connected is not already part of another path: connect/3
% 	- TO DO: the resulting State provides the best estimated cost based on the used heuristic, implement A*
% mve(+PairsList, +State, -NextState)
move([[], Board, SolPath], NextState):- % we reach base case once PairsList is empty, all thius can be moved in the goal check
	NextState = [[], Board, SolPath],	!.
move([[[_,X,Y,X1,Y1]|RemainingPairs], Board, Sol], NextState):-
	find_path((X,Y), (X1,Y1), [(X,Y)], Board, [], SolPath),
	reverse(SolPath, RevSolPath), % reverse the path once found
	move([RemainingPairs, Board, [RevSolPath|Sol]], NextState).
	
% Finds a direct connection path for adjacent cells (the base case), or
% an indirect connection by finding a possible path through an intermediate Via cell recursively
% Each possible paths are being collected in an Out list
% find_path(+From, +Dest, ?Board, +In, -Out)
find_path(From, Dest, Path, Board, In, In):-
    adjacent(From, Dest),
	\+ member(Dest, Path), 			% avoid cutting the same path again
    connect(From, Dest, Board).	% mark the cells being connected with same Nbr
find_path(From, Dest, Path, Board, _, Out):-
    adjacent(From, Via),
	\+ member(Via, Path), 			% avoid cutting the same path again
    connect(From, Via, Board),	% mark the cells being connected with same Nbr
	PossiblePath = [Via|Path],
    find_path(Via, Dest, PossiblePath, Board, [Dest|PossiblePath], Out).

% ===========================================================================
% GOAL CHECK
% ===========================================================================

% The goal determines if there are no free atoms left on the board
%	(all paths are already disjoint / do not intersect, and the numbers were all connected at this point)
% goal(+State)
goal([[], Board, _]):-
	ground(Board).
	
% ===========================================================================
% HELPERS TO PARSE INPUT AND PRODUCE USABLE REPRESENTATION OF A STATE (Board)
% ===========================================================================

% Reads a file and produces a list by calling stream_to_list/2
% read_file(+File, -List)
read_file(File, List):-
	open(File, read, Stream),
	stream_to_list(Stream, List),
	close(Stream).

% Read a file stream and produce a list with its content
% stream_to_list(+Stream, -List)
stream_to_list(Stream,[]) :-
    at_end_of_stream(Stream), !.
stream_to_list(Stream, [X|L]) :-
    \+ at_end_of_stream(Stream),
    read_line_to_codes(Stream, Codes),
    atom_chars(X, Codes),
    stream_to_list(Stream, L).

% Retrieve the parameters from the input: N is the size of the puzzle, K is the number of pairs, 
% then calls get_pairs/2 to retrieve the PairsList
% convert_input(+List, -BoardDimension, -PathCount, -PairsList)
convert_input(L, N_int, K_int, PairsList):-
	term_string(L, List),
	split_string(List, ['(',')',':',','], " ", L1),
	[_,N,K,_|Rest] = L1,
	atom_number(N, N_int),
	atom_number(K, K_int),
	get_pairs(Rest, PairsList).
	
% Retrieve the number Pk and the corresponding pairs for start (X,Y) and detination (X1,Y1)
% conversion to integer is perfoirmed on each of the atoms
% get_pairs(+InputList, -PairsList)
get_pairs([],[]).
get_pairs([_,Pk,_,X,Y,_,_,X1,Y1,_,_,_|Rest],[[Pk_int, X_int, Y_int, X1_int, Y1_int]|PairsList]):-
	atom_number(Pk, Pk_int),
	atom_number(X, X_int),
	atom_number(Y, Y_int),
	atom_number(X1, X1_int),
	atom_number(Y1, Y1_int),
	get_pairs(Rest, PairsList).

% Makes an empty board of corresponding size	
% empty_board(+Size, -Board)
empty_board(Size, Board):-
	empty_list(Size, Board),
	maplist(empty_list(Size), Board).
empty_list(Size, List):-length(List, Size).

% Fills cells with number values from the imput pairs
% fill_cells(+PairsList, -Board)
fill_cells([], _).
fill_cells([[Nbr,X,Y,X1,Y1]|Rest], Board):-
	nth1(Y, Board, Row),
    nth1(X, Row, Nbr),
	nth1(Y1, Board, Row1),
    nth1(X1, Row1, Nbr),
	fill_cells(Rest, Board).
	
% ===========================================================================
% HELPERS TO CONVER THE SOLUTION AND WRITE IT TO FILE
% ===========================================================================

% Put together the solution content by first adding the board dimensions and the number of paths,
% then call the assemble_paths/3 to addition the paths
% assemble_solution(+Sol, +BoardDimension, +PathCount, -SolutionOutput)
assemble_solution(Sol, N, K, [N,K|FinalSolution]):-
	reverse(Sol, SolutionPath),
	assemble_paths(SolutionPath, FinalSolution, 0).
	
% Succesively assemble each of the paths of the solution by adding the path number
% assemble_paths(+InPathsList, -OutAssembledPathsList, +Counter)
assemble_paths([],[],_).
assemble_paths([SubPath|RemainingPaths], [[Nbr : SubPath]|OutputList], Count):-	
	Nbr is Count + 1,
	assemble_paths(RemainingPaths, OutputList, Nbr).

% Write to file the formatted solution. First add the the board dimensions and the number of paths,
% then write the formatted paths line by line using write_lines/2
% write_file(?SolutionFile, +AssembledSolutionList)
write_file(SolutionFile, [N,K|PathsList]):-
	open(SolutionFile, write, Stream),
	format(Stream, '(~d, ~d', [N, K]),
    write_lines(Stream, PathsList),
	write(Stream, ')'),
    close(Stream).

% Each of the paths is converted to string, a few characters are replaced,
% then the string is written to stream line by line
% write_lines(?Stream, +PathsList)
write_lines(_, []).
write_lines(Stream, [H|Rest]):-
		[Nbr : SubPath] = H,
		term_string(SubPath, S),
		re_replace("\\[", "", S, S1),
		re_replace("\\]", ")", S1, S2),
		re_replace("\\),\\("/g, "), (", S2, S3),
		write(Stream, ',\n'),		
		format(Stream, '(~d: ', [Nbr]),
		write(Stream, S3),
        write_lines(Stream, Rest).
