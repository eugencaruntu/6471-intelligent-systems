:- use_module(library(pcre)).
:- use_module(library(clpfd)).

play(String, Size, Paths, PathsList):-
	clean(String, CleanedList),
	[Size, Paths | PathsList] = CleanedList.
	%disjoint(PathsList).
	
% Constraint 1: allowed 'moves' only up/down or left/right
Adjacent([[X|Y], [X1|Y]]) :- X is X1 +1; X is X1 -1. 
%Adjacent([[X|Y], [X|Y1]]) :- Y is Y1 +1; Y is Y1 -1.

/*
 a move is valid if:
	- the resulting Node (State) is created adding an adjacent cell
	- the cell being added is not already part of another path (the cell is not already in the list of cells for other paths)
	- the resulting Node provides the best estimated cost based on the used heuristic

move(Node, Child, Cost):-
	Adjacent(),
	NotMember(),
	LeastCost().


% The goal checks for:
%		- all paths to be disjoint
%		- the sum of paths' length to match the board size^2
goal(Node):-
	disjoint(PathsList),
	using_all_cells(BoardSize, PathsList).

using_all_cells(BoardSize, PathsList):-
	length(PathsList) == BoardSize ** 2.
	
*/	
% if all replacements were done, finally make a list out of the cleaned string
clean(String, Result):-
	not(sub_string(String, _, 1, _, "(")), 
	not(sub_string(String, _, 1, _, ")")),
	not(sub_string(String, _, 1, _, " ")),
	not(sub_string(String, _, 1, _, ":")),
	not(sub_string(String, _, 1, _, "\n")),
	term_to_atom(Result, String).

clean(String, Result):-
	sub_string(String, _, 1, _, "("),
	re_replace("\\(", "[", String, Result1),
	clean(Result1, Result),!.
	
clean(String, Result):-
	sub_string(String, _, 1, _, ")"),
	re_replace("\\)", "]", String, Result1),
	clean(Result1, Result).
	
clean(String, Result):-
	sub_string(String, _, 1, _, "\n"),
	re_replace("\n", "", String, Result1),
	clean(Result1, Result).
	
clean(String, Result):-
	sub_string(String, _, 1, _, " "),
	re_replace("\s", "", String, Result1),
	clean(Result1, Result).
		
clean(String, Result):-
	sub_string(String, _, 1, _, ":"),
	re_replace("\\d:", "", String, Result1),
	clean(Result1, Result).


/* ================================================
	
play("(7, 5,
(1: (3,6), (5,3)),
(2: (5,2), (1,7)),
(3: (2,2), (4,3)),
(4: (4,1), (5,7)),
(5: (6,2), (4,4)))", Size, Paths, PathsList).

[[[3, 6], [5, 3]], [[5, 2], [1, 7]], [[2, 2], [4, 3]], [[4, 1], [5, 7]], [[6, 2], [4, 4]]]


cell(X,Y).
path(Node1, Node2).
*/