play(Board_size, Tuples_count, Tuples, Result):-
	% place all the board cells in the Board list (this will represent the unvisited list of cells)
	populateBoard(Board_size, Tuples_count, Board),
	

/*
cell(X,Y).
path(Node1, Node2).

% Constraint 1: allowed 'moves' only up/down or left/right
Adjacent(cell(X,Y), cell(X1,Y)) :- X is X1+1; X is X1-1. 
Adjacent(cell(X,Y), cell(X,Y1)) :- Y is Y1+1; Y is Y1-1. 

/* a move is valid if:
	- the resulting Node (State) is created adding an adjacent cell
	- the cell being added is not already part of another path (the cell is not already in the list of cells for other paths)
	- the resulting Node provides the best estimated cost based on the used heuristic
*/
move(Node, Child, Cost):-
	Adjacent(),
	NotMember(),
	LeastCost().


% The goal checks for:
%		- all paths to be disjoint
%		- the sum of paths' length to match the board size^2
goal(Node):-
	disjoint(PathsList),
	using_all_cells(BoardSize, PathsList).

using_all_cells(BoardSize, PathsList):-
	length(PathsList) == BoardSize ** 2.
	

% heuristic function applied on a particular state
cost(+Node, -Cost).

list_empty(List):-
	[] = List.

complete_list(List):-
	first(N1, List) i

first(F, [F|_]).
last(L, [H|T]) :- last(L, T),

complete_path(Visited):-
	last(L, L
	first(E, Visited)
	
connect(N, N1, N_Visited):-


% 


/* 

% INPUT
(7, 5,
(1: (3,6), (5,3)),
(2: (5,2), (1,7)),
(3: (2,2), (4,3)),
(4: (4,1), (5,7)),
(5: (6,2), (4,4)))

String = '(7, 5,
(1: (3,6), (5,3)),
(2: (5,2), (1,7)),
(3: (2,2), (4,3)),
(4: (4,1), (5,7)),
(5: (6,2), (4,4)))'.

% re_replace(+Pattern, +With, +String, -NewString)
re_replace("\\(\d:", "[", String, Result)

split_string("(7, 5,
(1: (3,6), (5,3)),
(2: (5,2), (1,7)),
(3: (2,2), (4,3)),
(4: (4,1), (5,7)),
(5: (6,2), (4,4)))", "\n", "", L).


% OUTPUT
(7, 5,
(1: (3,6), (3,5), (4,5), (5,5), (5,4), (5,3)),
(2: (5,2), (4,2), (3,2), (3,1), (2,1), (1,1), (1,2), (1,3), (1,4), (1,5), (1,6), (1,7)),
(3: (2,2), (2,3), (3,3), (4,3)),
(4: (4,1), (5,1), (6,1), (7,1), (7,2), (7,3), (7,4), (7,5), (7,6), (7,7), (6,7), (5,7)),
(5: (6,2), (6,3), (6,4), (6,5), (6,6), (5,6), (4,6), (4,7), (3,7), (2,7), (2,6), (2,5), (2,4), (3,4),(4,4)))

 */
atomic_list_concat(L, ' ', A).


play(7, 5,
[ [(3,6), (5,3)],
[(5,2), (1,7)],
[(2,2), (4,3)],
[(4,1), (5,7)],
[(6,2), (4,4)] ])
*/









