:- use_module(library(readln)).
:- prompt(_, 'Please input the problem: ').

read_input(X) :-
	readln(X), nl,
	write('You entered these lines:'), nl,
	write(X).
