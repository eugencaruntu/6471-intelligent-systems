
% make the board and populate it with the start, end pairs
populate(InputPairList, Size, Board):-
	empty_board(Size, Board),
	fill_cells(InputPairList, Board),

% makes an empty board of corresponding size
empty_board(Size, Board):-
	G is Size * Size,
	length(Flat, G),										% a flat list of empty elements
	make_empty_board(Flat, Size, Board). 	% make a List of lists (or matrix) from it
make_empty_board([], _, []).
make_empty_board(List, Size, [H|T]) :-
    append(H, Rest, List),
    length(H, Size),
    make_empty_board(Rest, Size, T).

% fill cells with values
fill_cells([], Board).
fill_cells([[(X,Y,Nbr)|(X1,Y1,Nbr)]|Rest], Board):-
    nth1(Y, Board, Row),
    nth1(X, Row, (X,Y,Nbr)),
	nth1(Y1, Board, Row1),
    nth1(X1, Row1, (X1,Y1,Nbr))
	fill_cells(Rest, Board).