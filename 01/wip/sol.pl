% Constraint 1: allowed connections are permitted on adjacent cells (only up/down or left/right)
adjacent((X,Y1,_), (X,Y,_)) :- Y is Y1 +1; Y is Y1 -1.
adjacent((X1,Y,_), (X,Y,_)) :- X is X1 +1; X is X1 -1.

%  Constraint 2: Two cells can be connected if they have the same number:
% - get the Yth sublist within the State
% - the Xth cell on the sublist that has the same number
connect((X,Y,Nbr), (X1,Y1,Nbr1), State):-
	Nbr = Nbr1.
  %  nth1(Y, State, Sublist),
  %  nth1(X, Sublist, (X,Y,Nbr)),
  %  nth1(Y1, State, Sublist1),
   % nth1(X1, Sublist1, (X1,Y1,Nbr)).
	
% Direct connection of adjacent cells (the base case)
find_path(From, Dest, Path, State):-
    adjacent(From, Dest),
	\+ member(Dest, Path), % avoid cutting the same path again
    connect(From, Dest, State).
	
% Indirect connection through an intermediate cell:
% check if there is a possible path on the connected cells when Via cell is added to the Path
find_path(From, Dest, Path, State):-
    adjacent(From, Via),
	\+ member(Via, Path), % avoid cutting the same path again
    connect(From, Via, State),
	PossiblePath = [Via|Path],
    find_path(Via, Dest, PossiblePath, State). 

% for each list in the state
 move([], NextState).
 move(State, NextState):-
	[[From|Dest]|Rest] = State,
	find_path(From, Dest, [From], NextState).
/*
play(InputPaths):-
	InputPaths = [[(3,6),(5,3)],[(5,2),(1,7)],[(2,2),(4,3)],[(4,1),(5,7)],[(6,2),(4,4)]].
	
	move(State, NextState)
	.
*/

goal(Board):-
  % Lenght of solution matches 
 % using_all_cells(BoardSize, PathsList),
  Board = [[_,_,_,(4,1,4),_,_,_],
         [_,(2,2,3),_,_,(5,2,2),(6,2,5),_],
         [_,_,_,(4,3,3),(5,3,1),_,_],
         [_,_,_,(4,4,5),_,_,_],
         [_,_,_,_,_,_,_],
         [_,_,(3,6,1),_,_,_,_],
         [(1,7,2),_,_,_,(5,7,4),_,_]],
    find_path((3,6,1), (5,3,1), [(3,6,1)], Board),
    find_path((5,2,2), (1,7,2), [(5,2,2)], Board),
    find_path((2,2,3), (4,3,3), [(2,2,3)], Board),
    find_path((4,1,4), (5,7,4), [(4,1,4)], Board),
    find_path((6,2,5), (4,4,5), [(6,2,5)], Board).

/*
using_all_cells(BoardSize, PathsList):-
	get_lenght(PathsList) == BoardSize ** 2.

% get the lenght of a list of lists
get_lenght([], 0).
get_lenght([H|T], Len) :-
	length(H, Tmp1),
	get_lenght(T, Tmp2),
	Len is Tmp1 + Tmp2.
	*/
	