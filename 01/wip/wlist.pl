:-dynamic(hi/1).
test :-
    hi(pippo),
    hi(george).

write_result(Term) :-
    
    term_to_atom(Term, Atom),
    atom_codes(Atom, String),
    
    replace("(", String, " ", NewString1),
    replace(")", NewString1, ".", ResultString),
    atom_codes(Result, ResultString),
    write(Result).

write_body(N,(First,Rest)) :-
    tab(N),
    write_result(First),
    nl,
    write_body(N,Rest).
write_body(N,Last) :-
    tab(N),
    write_result(Last),
    nl.

how(Goal) :-
    clause(Goal,Body),
    write_body(4,Body).