/* Improved PCFG based mostly on parent annotations for NP, PP and lexicalization for V */

/* Adjusted probabilities to account for usage of "take" and "put" in following 4 production rules */

s(P0, s(V, NP)) --> v_take(P1, V), np_s(P2, NP), {P0 is P1*P2*0.3499}.
/* There is no instance in training where "take" uses PP from S, but we keep a smoothing*/
s(P0, s(V, NP, PP)) --> v_take(P1, V), np_s(P2, NP), pp_s(P3, PP), {P0 is P1*P2*P3*0.0001}.

s(P0, s(V, NP, PP)) --> v_put(P1, V), np_s(P2, NP), pp_s(P3, PP), {P0 is P1*P2*P3*0.6499}.
/* There is no instance in training where "put" is used without a PP, but we keep a smoothing*/
s(P0, s(V, NP)) --> v_put(P1, V), np_s(P2, NP), {P0 is P1*P2*0.0001}.


/* NP with S parent annotation */
np_s(P0, np(D, N)) --> det_np(P1, D), n_np(P2, N), {P0 is P1*P2*0.36}.
np_s(P0, np(D, A, N)) --> det_np(P1, D), a_np(P2, A), n_np(P3, N), {P0 is P1*P2*P3*0.46}.
np_s(P0, np(D, N, PP)) --> det_np(P1, D), n_np(P2, N), pp_np(P3, PP), {P0 is P1*P2*P3*0.13}.
np_s(P0, np(D, A, N, PP)) --> det_np(P1, D), a_np(P2, A), n_np(P3, N), pp_np(P4, PP), {P0 is P1*P2*P3*P4*0.05}.

/* NP with PP parent annotation */
np_pp(P0, np(D, N)) --> det_np(P1, D), n_np(P2, N), {P0 is P1*P2*0.36}.
np_pp(P0, np(D, A, N)) --> det_np(P1, D), a_np(P2, A), n_np(P3, N), {P0 is P1*P2*P3*0.63}.
np_pp(P0, np(D, N, PP)) --> det_np(P1, D), n_np(P2, N), pp_np(P3, PP), {P0 is P1*P2*P3*0.005}.
np_pp(P0, np(D, A, N, PP)) --> det_np(P1, D), a_np(P2, A), n_np(P3, N), pp_np(P4, PP), {P0 is P1*P2*P3*P4*0.005}.

/* PP with S parent annotation */
pp_s(P0, pp(P, NP)) --> p_pp(P1, P), np_pp(P2, NP), {P0 is P1*P2*1.0}.

/* PP with NP parent annotation */
pp_np(P0, pp(P, NP)) --> p_pp(P1, P), np_pp(P2, NP), {P0 is P1*P2*1.0}.

/* V with lexicalization for "take" and "put" */
v_take(P0, V) --> v(P1, V), {V = v(take), P0 is P1*1.0}.  % [take]
v_put(P0, V) --> v(P1, V), {V = v(put), P0 is P1*1.0}.    % [put]

/* Remainder of other trivial parent annotation */
det_np(P0, D) --> det(P1, D), {P0 is P1*1.0}.   % [the].
a_np(P0, A) --> a(P1, A), {P0 is P1*1.0}.       % [blue, green, red]
n_np(P0, N) --> n(P1, N), {P0 is P1*1.0}.       % [block, circle, cone, cube, square]
p_pp(P0, P) --> p(P1, P), {P0 is P1*1.0}.       % [on].

/* Terminals / Lexic */
v(0.65, v(put)) --> [put].
v(0.35, v(take)) --> [take].

n(0.23, n(block)) --> [block].
n(0.25, n(circle)) --> [circle].
n(0.15, n(cone)) --> [cone].
n(0.12, n(cube)) --> [cube].
n(0.25, n(square)) --> [square].

a(0.56, a(blue)) --> [blue].
a(0.27, a(green)) --> [green].
a(0.17, a(red)) --> [red].

det(1.0, det(the)) --> [the].

p(1.0, p(on)) --> [on].
